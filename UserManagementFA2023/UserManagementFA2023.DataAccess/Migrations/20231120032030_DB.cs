﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace UserManagementFA2023.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class DB : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Timelines_Units_UnitId",
                table: "Timelines");

            migrationBuilder.DropIndex(
                name: "IX_Timelines_UnitId",
                table: "Timelines");

            migrationBuilder.DropColumn(
                name: "UnitId",
                table: "Timelines");

            migrationBuilder.AddColumn<int>(
                name: "TimeLineId",
                table: "Units",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetRoles",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Units_TimeLineId",
                table: "Units",
                column: "TimeLineId");

            migrationBuilder.AddForeignKey(
                name: "FK_Units_Timelines_TimeLineId",
                table: "Units",
                column: "TimeLineId",
                principalTable: "Timelines",
                principalColumn: "TimeLineId",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Units_Timelines_TimeLineId",
                table: "Units");

            migrationBuilder.DropIndex(
                name: "IX_Units_TimeLineId",
                table: "Units");

            migrationBuilder.DropColumn(
                name: "TimeLineId",
                table: "Units");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetRoles");

            migrationBuilder.AddColumn<int>(
                name: "UnitId",
                table: "Timelines",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Timelines_UnitId",
                table: "Timelines",
                column: "UnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_Timelines_Units_UnitId",
                table: "Timelines",
                column: "UnitId",
                principalTable: "Units",
                principalColumn: "UnitId");
        }
    }
}
