﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace UserManagementFA2023.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class UserManagement_DB : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Timelines_Units_UnitId",
                table: "Timelines");

            migrationBuilder.RenameColumn(
                name: "UnitId",
                table: "Timelines",
                newName: "DayId");

            migrationBuilder.RenameIndex(
                name: "IX_Timelines_UnitId",
                table: "Timelines",
                newName: "IX_Timelines_DayId");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetRoles",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "Days",
                columns: table => new
                {
                    DayId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Datetime = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UnitId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Days", x => x.DayId);
                    table.ForeignKey(
                        name: "FK_Days_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "UnitId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Days_UnitId",
                table: "Days",
                column: "UnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_Timelines_Days_DayId",
                table: "Timelines",
                column: "DayId",
                principalTable: "Days",
                principalColumn: "DayId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Timelines_Days_DayId",
                table: "Timelines");

            migrationBuilder.DropTable(
                name: "Days");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetRoles");

            migrationBuilder.RenameColumn(
                name: "DayId",
                table: "Timelines",
                newName: "UnitId");

            migrationBuilder.RenameIndex(
                name: "IX_Timelines_DayId",
                table: "Timelines",
                newName: "IX_Timelines_UnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_Timelines_Units_UnitId",
                table: "Timelines",
                column: "UnitId",
                principalTable: "Units",
                principalColumn: "UnitId");
        }
    }
}
