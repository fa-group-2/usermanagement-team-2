﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.DataAccess.Data;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models;

namespace UserManagementFA2023.DataAccess.Repository
{
    public class StatusRepository : Repository<Status>, IStatusRepository
    {
        private AppDbContext _db;
        private IMapper _mapper;
        public IStatusRepository status { get; private set; }

        public StatusRepository(AppDbContext db, IMapper mapper) : base(db, mapper)
        {
            _db = db;
            _mapper = mapper;
        }
    }
}
