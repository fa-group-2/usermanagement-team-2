﻿using AutoMapper;
using ClosedXML.Excel;
using CsvHelper;
using CsvHelper.Configuration;
using DocumentFormat.OpenXml.InkML;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.EntityFrameworkCore;
using System.Formats.Asn1;
using System.Globalization;
using System.Text;
using UserManagementFA2023.DataAccess.Data;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;
using UserManagementFA2023.Utility;

namespace UserManagementFA2023.DataAccess.Repository
{
    public class SyllabusRepository : Repository<Syllabus>, ISyllabusRepository
    {
        private AppDbContext _db;
        private IMapper _mapper;
        public ISyllabusRepository syllabus { get; private set; }

        public SyllabusRepository(AppDbContext db, IMapper mapper) : base(db, mapper)
        {
            _db = db;
            _mapper = mapper;
        }
        public async Task<byte[]> DownloadSyllabusCSVTemplate()
        {
            var headers = new List<string>
            {
                "Syllabus ID",
                "Syllabus Title",
                "Syllabus Code",
                "Date Created",
                "User ID",
                "Duration",
                "Status ID",
                "Assessment ID",
                "Principle ID"
            };

            var csvContent = new StringBuilder();
            csvContent.AppendLine(string.Join(",", headers));

            var csvBytes = Encoding.UTF8.GetBytes(csvContent.ToString());

            return csvBytes;
        }

        public async Task<byte[]> ExportAllSyllabusCSV()
        {
            var allSyllabus = await _db.Syllabus.ToListAsync();

            if (allSyllabus == null || allSyllabus.Count == 0)
            {
                return null;
            }

            var headers = new List<string>
            {
                "Syllabus ID",
                "Syllabus Title",
                "Syllabus Code",
                "Date Created",
                "User ID",
                "Duration",
                "Status ID",
                "Assessment ID",
                "Principle ID"
            };

            var csvContent = new StringBuilder();
            csvContent.AppendLine(string.Join(",", headers));

            foreach (var syllabus in allSyllabus)
            {
                var values = new List<string>
                {
                    syllabus.SyllabusId.ToString(),
                    syllabus.SyllabusTittle,
                    syllabus.SyllabusCode,
                    syllabus.CreatedDate.ToString(),
                    syllabus.Id,
                    syllabus.Duration.ToString(),
                    syllabus.StatusId.ToString(),
                    syllabus.AssessId.ToString(),
                    syllabus.PrincipleId.ToString()
                };

                csvContent.AppendLine(string.Join(",", values));
            }

            var csvBytes = Encoding.UTF8.GetBytes(csvContent.ToString());
            return csvBytes;
        }   
        public async Task<byte[]?> ExportSyllabusCSV(int syllabusId)
        {
            var syllabus = await _db.Syllabus.FindAsync(syllabusId);

            if (syllabus == null)
            {
                return null;
            }
            var syllabusDetailId = _db.SyllabusDetails
                .FirstOrDefault(d => d.SyllabusId == syllabus.SyllabusId);
            var syllabusDetail = await _db.SyllabusDetails.FindAsync(syllabusDetailId.SyllabusDetailId);

            if (syllabusDetail == null)
            {
                return null;
            }
            var headers = new List<string>
            {
                "Syllabus ID",  
                "Syllabus Title",
                "Syllabus Code",
                "Date Created",
                "User ID",
                "Duration",
                "Status ID",
                "Assessment ID",
                "Principle ID"
            };

            var csvContent = new StringBuilder();
            csvContent.AppendLine(string.Join(",", headers));

            var values = new List<string>
            {
                syllabus.SyllabusId.ToString(),
                syllabus.SyllabusTittle,
                syllabus.SyllabusCode,
                syllabus.CreatedDate.ToString(),
                syllabus.Id,
                syllabus.Duration.ToString(),
                syllabus.StatusId.ToString(),
                syllabus.AssessId.ToString(),
                syllabus.PrincipleId.ToString()
            };

            csvContent.AppendLine(string.Join(",", values));

            var csvBytes = Encoding.UTF8.GetBytes(csvContent.ToString());
            return csvBytes;
        }
        public async Task<ResponseDTO> ImportSyllabusCSV(string csvText)
        {
            var records = new List<Syllabus>();

            using (var reader = new StringReader(csvText))
            using (var csv = new CsvReader(reader, new CsvConfiguration(CultureInfo.InvariantCulture)))
            {
                csv.Read();
                csv.ReadHeader();
                while (csv.Read())
                {
                    var syllabus = new Syllabus
                    {
                        //SyllabusId = csv.GetField<int>("Syllabus ID"),
                        SyllabusTittle = csv.GetField<string>("Syllabus Title"),
                        SyllabusCode = csv.GetField<string>("Syllabus Code"),
                        CreatedDate = DateTime.Parse(csv.GetField<string>("Date Created")),
                        Id = csv.GetField<string>("User ID"),
                        Duration = csv.GetField<int>("Duration"),
                        StatusId = csv.GetField<int>("Status ID"),
                        AssessId = csv.GetField<int>("Assessment ID"),
                        PrincipleId = csv.GetField<int>("Principle ID")
                    };
                    records.Add(syllabus);
                }
            }

            if (records.Count == 0)
            {
                return new ResponseDTO { Message = "No records found in the CSV.", IsSuccess = false };
            }

            foreach (var syllabus in records)
            {
                _db.Syllabus.Add(syllabus);
            }

            await _db.SaveChangesAsync();

            return new ResponseDTO { Message = $"Successfully imported {records.Count} syllabus records.", IsSuccess = true };
        }
    
        public async Task<ResponseDTO> DuplicatedSyllabus(int id)
        {
            var obj = _db.Syllabus.FirstOrDefault(x => x.SyllabusId == id); ;
            if (obj != null)
            {
                var duplicateObj = new Syllabus
                {
                    SyllabusTittle = obj.SyllabusTittle,
                    SyllabusCode = obj.SyllabusCode,
                    CreatedDate = obj.CreatedDate,
                    Id = obj.Id,
                    User = obj.User,
                    Duration = obj.Duration,
                    Status = obj.Status,
                };
                _db.Syllabus.Add(duplicateObj);
            }
            await _db.SaveChangesAsync();

            return new ResponseDTO { Message = "Syllabus duplicated successfully.", IsSuccess = true };
        }
        public async Task<ResponseDTO> ChangeStatus(int id)
        {
            var obj = await _db.Syllabus.FirstOrDefaultAsync(x => x.SyllabusId == id);
            if (obj != null)
            {
                obj.Status!.IsActive = false;
                await _db.SaveChangesAsync();
                return new ResponseDTO { Message = "Syllabus Delete successfully.", IsSuccess = true };
            }
            else
            {
                return new ResponseDTO { Message = "Syllabus not found.", IsSuccess = false };
            }
        }
        public async Task<ResponseDTO> Restore(int id)
        {
            var obj = await _db.Syllabus.FirstOrDefaultAsync(x => x.SyllabusId == id);
            if (obj != null)
            {
                obj.Status!.IsActive = true;
                await _db.SaveChangesAsync();
                return new ResponseDTO { Message = "Syllabus restore successfully.", IsSuccess = true };
            }
            else
            {
                return new ResponseDTO { Message = "Syllabus not found.", IsSuccess = false };
            }
        }
    }
}