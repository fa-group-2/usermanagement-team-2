﻿using AutoMapper;
using DocumentFormat.OpenXml.Office2010.Excel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.DataAccess.Data;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.DataAccess.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly AppDbContext _db;
        private ResponseDTO _response;
        private readonly DbSet<T> _set;
        private readonly IMapper _mapper;
        public Repository(AppDbContext db, IMapper mapper)
        {
            _db = db;
            _response = new ResponseDTO();
            this._set = _db.Set<T>();
            _mapper = mapper;
        }

        public async Task<ResponseDTO> GetAll()
        {
            try
            {
                _response.Result =  _set.ToList();
            }
            catch (Exception ex)
            {
                _response.Message = ex.Message;
                _response.IsSuccess = false;
            }
            return _response;
        }
        public IEnumerable<T> GetAllClass()
        {
            return _db.Set<T>().ToList();
        }
        public async Task<ResponseDTO> GetById(int id)
        {
            try
            {
                _response.Result =  _set.Find(id);
            }
            catch (Exception ex)
            {
                _response.Message = ex.Message;
                _response.IsSuccess = false;
            }
            return _response;
        }

        public async Task<ResponseDTO> Create(T entity)
        {
            try
            {
                 _set.Add(entity);
                 _db.SaveChangesAsync();
                _response.Message = "Add successfully";
            }
            catch (Exception ex)
            {
                _response.Message = ex.Message;
                _response.IsSuccess = false;
            }
            return _response;
        }

        public async Task<ResponseDTO> Update(T entity)
        {
            try
            {
                _set.Update(entity);
                 _db.SaveChanges();
                _response.Message = "Updated successfully";
            }
            catch (Exception ex)
            {
                _response.Message = ex.Message;
                _response.IsSuccess = false;
            }
            return _response;
        }

        public async Task<ResponseDTO> DeleteById(int id)
        {
            try
            {
                var obj =  _set.Find(id);

                if (obj != null)
                {
                    _set.Remove(obj);
                     _db.SaveChanges();
                    _response.Message = "Deleted successfully";
                }
                else
                {
                    _response.Message = "Object not found";
                    _response.IsSuccess = false;
                }
            }
            catch (Exception ex)
            {
                _response.Message = ex.Message;
                _response.IsSuccess = false;
            }

            return _response;
        }
        public async Task<ResponseDTO> GetByName(string name)
        {
            try
            {
                _response.Result = _set.Find(name);
            } catch (Exception ex)
            {
                _response.Message = ex.Message;
                _response.IsSuccess = false;
            }
            return _response;
        }

        public IQueryable<T> FindAll(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> items = _db.Set<T>();
            if (includeProperties != null)
            {
                foreach (var includeProperty in includeProperties)
                {
                    items = items.Include(includeProperty);
                }
            }
            return items;
        }
    }
}
