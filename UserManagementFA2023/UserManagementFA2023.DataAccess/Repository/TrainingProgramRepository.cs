﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.DataAccess.Data;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.DTO;
using UserManagementFA2023.Models.Models;

namespace UserManagementFA2023.DataAccess.Repository
{
    public class TrainingProgramRepository : Repository<TrainningProgram>, ITrainingProgramRepository
    {
        private AppDbContext _db;
        private readonly IMapper _mapper;
        public TrainingProgramRepository(AppDbContext db, IMapper mapper) : base(db, mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task<ResponseDTO> ChangeStatus(int id)
        {
            var obj = await _db.TrainningPrograms.FirstOrDefaultAsync(x => x.TrainningProgramId == id);
            if (obj != null)
            {
                obj.Status!.IsActive = false;
                await _db.SaveChangesAsync();
                return new ResponseDTO { Message = "Training deleted successfully.", IsSuccess = true };
            }
            else
            {
                return new ResponseDTO { Message = "Training not found.", IsSuccess = false };
            }
        }

        public async Task<ResponseDTO> DuplicatedClass(int id)
        {
            var obj = _db.TrainningPrograms.FirstOrDefault(x => x.TrainningProgramId == id); ;
            if (obj != null)
            {
                var duplicateObj = new TrainningProgram
                {
                    TrainningProgramName = obj.TrainningProgramName,    
                    Id = obj.Id,  
                    Duration = obj.Duration,
                    StatusId = obj.StatusId, 
                };
                _db.TrainningPrograms.Add(duplicateObj);
                _db.SaveChanges();
            }
            return new ResponseDTO { Message = "Syllabus duplicated successfully.", IsSuccess = true };
        }

        public Task<ResponseDTO> ImportClass()
        {
            throw new NotImplementedException();
        }

        public async Task<ResponseDTO> Restore(int id)
        {
            var obj = await _db.TrainningPrograms.FirstOrDefaultAsync(x => x.TrainningProgramId == id);
            if (obj != null)
            {
                obj.Status!.IsActive = true;
                await _db.SaveChangesAsync();
                return new ResponseDTO { Message = "Training restore successfully.", IsSuccess = true };
            }
            else
            {
                return new ResponseDTO { Message = "Training not found.", IsSuccess = false };
            }
        }

        public async Task<ResponseDTO> DeactiveProgram(int id)
        {
            var obj = await _db.TrainningPrograms.FirstOrDefaultAsync(x => x.TrainningProgramId == id);
            if (obj != null)
            {
                if (obj.Status == null)
                {
                    obj.Status = new Status(); // Instantiate the Status object if it is null
                }

                obj.Status.IsActive = false;
                await _db.SaveChangesAsync();
                return new ResponseDTO { Message = "Deactive Training successfully.", IsSuccess = true };
            }
            else
            {
                return new ResponseDTO { Message = "Training not found.", IsSuccess = false };
            }
        }

        public async Task<ResponseDTO> ActiveProgram(int id)
        {
            var obj = await _db.TrainningPrograms.FirstOrDefaultAsync(x => x.TrainningProgramId == id);
            if (obj != null)
            {
                if (obj.Status == null)
                {
                    obj.Status = new Status(); // Instantiate the Status object if it is null
                }

                obj.Status.IsActive = true;
                await _db.SaveChangesAsync();
                return new ResponseDTO { Message = "Active Training successfully.", IsSuccess = true };
            }
            else
            {
                return new ResponseDTO { Message = "Training not found.", IsSuccess = false };
            }
        }

        public async Task<List<TrainningProgram>> SearchTrainingPrograms(string keyword)
        {
            var programs = await _db.TrainningPrograms
                .Where(p => p.TrainningProgramName.Contains(keyword))
                .ToListAsync();

            return programs;
        }
    }
}