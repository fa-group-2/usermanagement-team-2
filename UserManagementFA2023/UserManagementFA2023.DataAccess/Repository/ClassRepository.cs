﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.DataAccess.Data;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.DataAccess.Repository
{
    public class ClassRepository : Repository<Class>, IClassRepository
    {
        private AppDbContext _db;

        private IMapper _mapper;
        public ClassRepository(AppDbContext db, IMapper mapper) : base(db, mapper)
        {
            _mapper = mapper;
            _db = db;
        }

        public async Task<ResponseDTO> DuplicatedClass(int id)
        {
            var obj =  _db.Class.FirstOrDefault(x => x.ClassId == id); 
            if (obj != null)
            {
                var duplicateObj = _mapper.Map<Class, Class>(obj);
                 _db.Class.Add(duplicateObj);
                _db.SaveChanges();
            }
            return new ResponseDTO { Message = "Class duplicated successfully.", IsSuccess = true };
        }

        public Task<ResponseDTO> ImportClass(IFormFile file, int startRow, int startCloumn)
        {
            throw new NotImplementedException();
        }
        public async Task<ResponseDTO> GetAll(int page, int?[] locationId, DateTime? dateFrom, DateTime? dateTo, int?[] attendeeId, int? userId, int pageSize)
        {
            var classes =  _db.Class.AsQueryable();

            if (!locationId.IsNullOrEmpty())
            {
                classes = classes.Where(cls => locationId.Contains(cls.LocationId));
            }
            if (dateFrom != null && dateTo == null)
            {
                classes = classes.Where(cls => dateFrom >= cls.DateFrom);
            }
            else if (dateTo != null && dateFrom == null)
            {
                classes = classes.Where(cls => cls.DateTo >= dateTo);
            }
            else if (dateTo != null && dateFrom != null)
            {
                classes = classes.Where(cls => dateFrom >= cls.DateFrom && cls.DateTo >= dateTo);
            }
            if (!attendeeId.IsNullOrEmpty())
            {
                classes = classes.Where(cls => attendeeId.Contains(cls.LevelId));
            }

            var result = await PaginatedListDTO<Class>.Create(classes, page, pageSize);

            var pageNumber = result.TotalPage;



            return  new ResponseDTO { Result = _mapper.Map<List<ClassDTO>>(result), Message ="Total page: " + pageNumber, IsSuccess = true };

        }
	}
}
