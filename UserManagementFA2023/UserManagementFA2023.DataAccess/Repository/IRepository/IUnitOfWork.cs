﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagementFA2023.DataAccess.Repository.IRepository
{
    public interface IUnitOfWork 
    {
        public ILevelRepository levelRepository { get; }
        public IUserRepository userRepository { get; }
        public ILocationRepository locationRepository { get; }
        public ISyllabusRepository syllabusRepository { get; }
        public IClassRepository classRepository { get; }
        public IClassDetailRepository classDetailRepository { get; }
        public ITrainingProgramDetailsRepository trainingProgramDetailsRepository { get; }  
        public ITrainingProgramRepository trainingProgramRepository { get; }    
        public ISyllabusDetailsRepository syllabusDetailsRepository { get; }    
        public IMaterialRepository materialRepository { get; }
        public void Save();
    }
}
