﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.Models.DTO;
using UserManagementFA2023.Models.Models;

namespace UserManagementFA2023.DataAccess.Repository.IRepository
{
    public interface ITrainingProgramRepository : IRepository<TrainningProgram>
    {
        Task<ResponseDTO> ImportClass();
        Task<ResponseDTO> DuplicatedClass(int id);
        Task<ResponseDTO> ChangeStatus(int id);
        Task<ResponseDTO> Restore(int id);
        Task<ResponseDTO> ActiveProgram(int id);
        Task<ResponseDTO> DeactiveProgram(int id);
        Task<List<TrainningProgram>> SearchTrainingPrograms(string keyword);
    }
}