﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.DataAccess.Repository.IRepository
{
    public interface ISyllabusRepository : IRepository<Syllabus>
    {
        Task<byte[]> DownloadSyllabusCSVTemplate();
        Task<byte[]> ExportSyllabusCSV(int syllabusId);
        Task<byte[]> ExportAllSyllabusCSV();
        Task<ResponseDTO> ImportSyllabusCSV(string csvText);
        Task<ResponseDTO> DuplicatedSyllabus(int id);
        Task<ResponseDTO> ChangeStatus(int id);
        Task<ResponseDTO> Restore(int id);

    }
}
