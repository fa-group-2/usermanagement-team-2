﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.DataAccess.Repository.IRepository
{
    public interface IClassRepository : IRepository<Class>
    {
        Task<ResponseDTO> DuplicatedClass(int id);
        Task<ResponseDTO> ImportClass(IFormFile file, int startRow, int startColumn);
        Task<ResponseDTO> GetAll(int page, int?[] locationId, DateTime? dateFrom, DateTime? dateTo, int?[] attendeeId, int? userId, int pageSize);
    }
}
