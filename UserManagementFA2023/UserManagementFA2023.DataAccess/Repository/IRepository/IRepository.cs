﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.DataAccess.Repository.IRepository
{
    public interface IRepository<T> where T : class 
    {
        Task<ResponseDTO> GetAll();
        IEnumerable<T> GetAllClass();
        Task<ResponseDTO> GetById (int id);
        Task<ResponseDTO> GetByName(string name);

        Task<ResponseDTO> Create (T entity);    
        Task<ResponseDTO> Update(T entity);
        Task<ResponseDTO> DeleteById (int id);
        IQueryable<T> FindAll(params Expression<Func<T, object>>[] includeProperties);
    }
}
