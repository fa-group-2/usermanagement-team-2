﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.DataAccess.Repository.IRepository
{
    public interface IMaterialRepository : IRepository<MaterialResource>
    {
        Task<List<MaterialResource>> GetAllValidMaterials();
        Task<ResponseDTO> ChangeStatus(int id);
        Task<IActionResult> DownloadFileMaterialAsync(string filePath, int id);
        Task<bool> DeleteMaterialFile(int id, string rootPath);
    }
}
