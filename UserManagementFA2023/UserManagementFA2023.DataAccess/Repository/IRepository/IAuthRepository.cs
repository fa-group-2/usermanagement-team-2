﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.DataAccess.Repository.IRepository
{
    public interface IAuthRepository
    {
        Task<string> Register(RegisterationRequestDTO registerationRequestDTO);
        Task<LoginResponseDTO> Login(LoginRequestDTO loginRequestDTO);
        Task<bool> AssignRole(String email, string roleName);
        Task<ResponseDTO> ChangeUserRole(string UserId, string NewRole);
       
    }
}
