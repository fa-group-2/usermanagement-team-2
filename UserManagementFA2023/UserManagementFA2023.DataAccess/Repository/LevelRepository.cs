﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.DataAccess.Data;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.DataAccess.Repository
{
    public class LevelRepository : Repository<Level> , ILevelRepository
    {
        private AppDbContext _db;

        private IMapper _mapper;

        public LevelRepository(AppDbContext db, IMapper mapper): base(db,mapper)
        {
            _db = db;
            _mapper = mapper;
        }
    }
}
