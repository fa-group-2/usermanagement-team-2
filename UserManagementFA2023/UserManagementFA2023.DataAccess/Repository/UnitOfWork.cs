﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.DataAccess.Data;
using UserManagementFA2023.DataAccess.Repository.IRepository;

namespace UserManagementFA2023.DataAccess.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _db;
        private readonly IMapper _mapper;
        public ILevelRepository levelRepository { get; }
        public ILocationRepository locationRepository { get; }
        public ISyllabusRepository syllabusRepository { get; }
        public IClassRepository classRepository { get; }
        public IClassDetailRepository classDetailRepository { get; }

        public ITrainingProgramDetailsRepository trainingProgramDetailsRepository { get; }

        public ITrainingProgramRepository trainingProgramRepository { get; }

        public ISyllabusDetailsRepository syllabusDetailsRepository { get; }
        public IMaterialRepository materialRepository { get; }

        public IUserRepository userRepository { get; }

        public UnitOfWork(AppDbContext db, IMapper mapper)
        {
            _mapper = mapper;
            _db = db;
            levelRepository = new LevelRepository(_db, _mapper);
            userRepository = new UserRepository(_db, _mapper);
            locationRepository = new LocationRepository(_db, _mapper);
            syllabusRepository = new SyllabusRepository(_db, _mapper);
            syllabusDetailsRepository = new SyllabusDetailsRepository(_db, _mapper);
            materialRepository = new MaterialRepository(_db, _mapper);
            classRepository = new ClassRepository(_db, _mapper);
            classDetailRepository = new ClassDetailsRepository(_db, _mapper);
            trainingProgramRepository = new TrainingProgramRepository(_db, _mapper);
            trainingProgramDetailsRepository = new TrainingProgramDetailsRepository(_db, _mapper);
        }
        public void Save()
        {
            _db.SaveChanges();
        }
    }
}
