﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.DataAccess.Data;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models;

namespace UserManagementFA2023.DataAccess.Repository
{
    public class ClassDetailsRepository : Repository<ClassDetails>, IClassDetailRepository
    {
        public readonly AppDbContext _db;
        public readonly IMapper _mapper;
        public ClassDetailsRepository(AppDbContext db, IMapper mapper) : base(db,mapper)
        {
            _db = db;     
            _mapper = mapper;
        }
    }
}
