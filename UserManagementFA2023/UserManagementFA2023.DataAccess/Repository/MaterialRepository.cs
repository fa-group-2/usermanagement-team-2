﻿using AutoMapper;
using Azure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using UserManagementFA2023.DataAccess.Data;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.DataAccess.Repository
{
    public class MaterialRepository : Repository<MaterialResource>, IMaterialRepository
    {
        private readonly IMapper _mapper;
        private readonly AppDbContext _db;
       
        public MaterialRepository(AppDbContext db, IMapper mapper) : base(db, mapper)
        {
            _db = db;
            _mapper = mapper;
        }
        public Task<List<MaterialResource>> GetAllValidMaterials()
        {
            var listMaterials = _db.MaterialResource.Where(m => m.Status == true).ToListAsync();
            return listMaterials;
        }
        public async Task<IActionResult?> DownloadFileMaterialAsync(string filePath, int id)
        {
            string contentType;
            byte[] fileBytes;

            var provider = new FileExtensionContentTypeProvider();
            var material = await _db.MaterialResource.FindAsync(id);
            if (material == null)
            {
                return null;
            }

            var downloadMaterial = _mapper.Map<MaterialResourceDTO>(material);
            var downloadPath = Path.Combine(filePath, downloadMaterial.FileName);

            if (!provider.TryGetContentType(filePath, out contentType))
            {
                contentType = "application/octet-stream";
            }

            if (material.Status == false)
            {
                return null;
            }
            else
            {
                fileBytes = File.ReadAllBytes(downloadPath);

                return new FileContentResult(fileBytes, contentType)
                {
                    FileDownloadName = downloadMaterial.FileName
                };
            }
        }
        public async Task<ResponseDTO> ChangeStatus(int id)
        {
            var obj = await _db.MaterialResource.FirstOrDefaultAsync(x => x.MaterialId == id);
            if (obj != null)
            {
                obj.Status = false;
                await _db.SaveChangesAsync();
                return new ResponseDTO { Message = "Material Deleted Successfully.", IsSuccess = true };
            }
            else
            {
                return new ResponseDTO { Message = "Material not found.", IsSuccess = false };
            }
        }
        public async Task<bool> DeleteMaterialFile(int id, string rootPath)
        {
            var material = await _db.MaterialResource.FindAsync(id);
            if (material != null)
            {
                var filePath = Path.Combine(rootPath, material.FileName);
                File.Delete(filePath);

                material.Status = false;
                await _db.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }
}
