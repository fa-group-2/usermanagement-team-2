﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Stripe;
using UserManagementFA2023.Models;
using UserManagementFA2023.Models.Models;

namespace UserManagementFA2023.DataAccess.Data
{
    public class AppDbContext : IdentityDbContext<ApplicationUser>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<Syllabus> Syllabus { get; set; }
        public DbSet<SyllabusDetails> SyllabusDetails { get; set; }
        public DbSet<Class> Class { get; set; }
        public DbSet<ClassDetails> ClassDetails { get; set; }
        public DbSet<Lesson> Lessons { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Timeline> Timelines { get; set; }
        public DbSet<Attendence> Attendences { get; set; }
        public DbSet<Level> Levels { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<DeliveryPrinciple> DeliveryPrinciple { get; set; }
        public DbSet<AssessmentScheme> AssessmentScheme { get; set; }
        public DbSet<MaterialResource> MaterialResource { get; set; }
        public DbSet<MaterialType> MaterialTypes { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<EmailLogger> EmailLoggers { get; set; }
        public DbSet<TrainningProgram> TrainningPrograms { get; set; }
        public DbSet<TrainingProgramDetails> TrainingProgramDetails { get; set; }
        public DbSet<Permission> Permission { get; set; }   
        public DbSet<Functions> Functions { get; set; }
        public DbSet<Role>  Roles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ClassDetails>().HasKey(i => new
            {
                i.ClassId,
                i.Id
            });
            //        modelBuilder.Entity<ClassDetails>()
            //.HasOne(cd => cd.Location)
            //.WithMany()
            //.HasForeignKey(cd => cd.LocationId)
            //.OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Attendence>().HasKey(i => new
            {
                i.ClassId,
                i.UnitId
            });

            modelBuilder.Entity<Unit>()
                .HasOne(u => u.Timeline)
                .WithMany(t => t.Units)
                .HasForeignKey(u => u.TimeLineId);
            //        modelBuilder.Entity<Timeline>()
            //        .HasOne(t => t.Unit)
            //         .WithMany()
            //         .HasForeignKey(t => t.UnitId)
            //           .OnDelete(DeleteBehavior.Restrict);

        }

    }
}
