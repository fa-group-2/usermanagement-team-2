﻿using System.ComponentModel.DataAnnotations;

namespace UserManagementFA2023.Email.Models.DTO
{
    public class LocationDTO
    {
        public int LocationId { get; set; } 
        public string? LocationName { get; set; }    
        public string? District { get; set; }    
        public string? Province { get; set; }    
    }
}
