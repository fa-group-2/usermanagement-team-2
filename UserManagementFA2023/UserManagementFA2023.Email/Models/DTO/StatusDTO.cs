﻿using System.ComponentModel.DataAnnotations;

namespace UserManagementFA2023.Email.Models.DTO
{
    public class StatusDTO
    {
        public int StatusId { get; set; } 
        public bool? IsActive { get; set; }  
     
    }
}
