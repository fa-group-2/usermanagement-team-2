﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Email.Models.DTO
{
    public class ClassDetailsDTO
    {
        public int ClassId { get; set; }
        public ClassDTO? Class { get; set; }
        public string? Id { get; set; }
        public UserDTO? User { get; set; }
    }
}
