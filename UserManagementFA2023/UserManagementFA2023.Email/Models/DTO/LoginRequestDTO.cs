﻿namespace UserManagementFA2023.Email.Models.DTO
{
    public class LoginRequestDTO
    {
        public string Username { get; set; }    
        public string Password { get; set; }    
    }
}
