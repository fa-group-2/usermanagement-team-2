﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Email.Models.DTO
{
    public class TimelineDTO
    {
        public int TimeLineId { get; set; } 
        public int? SyllabusDetailId { get; set; }
        public SyllabusDetailsDTO? SyllabusDetails { get; set;}
        public string? Description { get; set; }
        public ICollection<UnitDTO> Units { get; set; }
        public int? MatertialTypeId { get; set; }
        public MaterialTypeDTO? MaterialType { get; set; }  
        
    }
}
