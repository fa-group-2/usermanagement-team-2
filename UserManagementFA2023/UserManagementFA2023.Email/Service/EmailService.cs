﻿using Microsoft.EntityFrameworkCore;
using System.Text;
using UserManagementFA2023.Email.Data;
using UserManagementFA2023.Email.Models.DTO;
using UserManagementFA2023.Email.Service.IService;

namespace UserManagementFA2023.Email.Services
{
	public class EmailService : IEmailService
    {
        private DbContextOptions<AppDbContext> _dbOptions;
        public EmailService(DbContextOptions<AppDbContext> dbOptions)
        {
            _dbOptions = dbOptions; 
        }
        public async Task EmailCreateClass(ClassDTO classDTO)
        {
            StringBuilder message = new StringBuilder();

            message.AppendLine("<br/>Class Email Requested ");
            message.AppendLine("<br/>Class created successfully: " + classDTO.ClassName);
            message.Append("<br/>");
            message.Append("<ul>");

            await LogAndEmail(message.ToString(), "admin@gmail.com");
        }
        public async Task EmailDeletedClass(int id)
        {
            StringBuilder message = new StringBuilder();

            message.AppendLine("<br/>Class Email Requested ");
            message.AppendLine("<br/>Class deleted successfully: " + id);
            message.Append("<br/>");
            message.Append("<ul>");

            await LogAndEmail(message.ToString(), "admin@gmail.com");
        }

        public async Task EmailCreateSyllabus(SyllabusDTO syllabusDTO)
        {
            StringBuilder message = new StringBuilder();

            message.AppendLine("<br/>Syllabus Email Requested ");
            message.AppendLine("<br/>Syllabus created successfully " + syllabusDTO.SyllabusTittle);;
            message.Append("<br/>");
            message.Append("<ul>");

            await LogAndEmail(message.ToString(), "admin@gmail.com");
        }
        public async Task EmailDeletedSyllabus(int id)
        {
            StringBuilder message = new StringBuilder();

            message.AppendLine("<br/>Syllabus Email Requested ");
            message.AppendLine("<br/>Syllabus has been deleted successfully " + id); ;
            message.Append("<br/>");
            message.Append("<ul>");

            await LogAndEmail(message.ToString(), "admin@gmail.com");
        }


        public async Task RegisterUserEmailAndLog(string email)
        {
            string message = "User Registeration Successful. <br/> Email : " + email;
            await LogAndEmail(message, "admin@gmail.com");
        }
        private async Task<bool> LogAndEmail(string message, string email)
        {
            try
            {
                EmailLogger emailLog = new()
                {
                    Email = email,
                    EmailSent = DateTime.Now,
                    Message = message
                };

                await using var _db = new AppDbContext(_dbOptions);
                _db.EmailLoggers.AddAsync(emailLog);
                await _db.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
