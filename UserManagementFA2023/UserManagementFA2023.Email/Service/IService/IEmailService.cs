﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.Email.Models.DTO;

namespace UserManagementFA2023.Email.Service.IService
{
    public interface IEmailService
    {
        Task RegisterUserEmailAndLog(string email);
        Task EmailCreateClass(ClassDTO classDTO);
        Task EmailCreateSyllabus(SyllabusDTO syllabusDTO);
    }
}
