﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using UserManagementFA2023.Email.Models.DTO;  
using UserManagementFA2023.Email.Services;

namespace UserManagementFA2023.Email.Messaging
{
    public class RabbitMQClassConsumer : BackgroundService
    {
        private readonly IConfiguration _configuration;
        private readonly EmailService _emailService;
        private IConnection _connection;
        private IModel _channel;

        public RabbitMQClassConsumer(IConfiguration configuration, EmailService emailService)
        {
            _configuration = configuration;
            _emailService = emailService;
            var factory = new ConnectionFactory
            {
                HostName = "localhost",
                UserName = "guest",
                Password = "guest",
            };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(_configuration.GetValue<string>("TopicAndQueueNames:ClassCreatedTopic"), false, false, false, null);
            _channel.QueueDeclare();
        }
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (ch, ea) =>
            {
                var content = Encoding.UTF8.GetString(ea.Body.ToArray());
                ClassDTO classDTO = JsonConvert.DeserializeObject<ClassDTO>(content);
                HandleMessage(classDTO).GetAwaiter().GetResult();

                _channel.BasicAck(ea.DeliveryTag, false);
            };

            _channel.BasicConsume(_configuration.GetValue<string>("TopicAndQueueNames:ClassCreatedTopic"), false, consumer);
            return Task.CompletedTask;
        }
        private async Task HandleMessage(ClassDTO classDTO)
        {
            _emailService.EmailCreateClass(classDTO).GetAwaiter().GetResult();
        }
    }
}
