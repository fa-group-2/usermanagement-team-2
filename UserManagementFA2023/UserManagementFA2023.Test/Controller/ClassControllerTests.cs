﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using UserManagementFA2023.API.Controller;
using UserManagementFA2023.API.RabbitMQSender;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.Test.Controller
{
    public class ClassControllerTests
    {
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly Mock<IRabbitMQControllerSenderMessageSender> _messageBusMock;
        private readonly Mock<IConfiguration> _configurationMock;
        private readonly ClassController _controller;

        public ClassControllerTests()
        {
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _messageBusMock = new Mock<IRabbitMQControllerSenderMessageSender>();
            _configurationMock = new Mock<IConfiguration>();
            _controller = new ClassController(_unitOfWorkMock.Object, _messageBusMock.Object, _configurationMock.Object);
        }

        [Fact]
        public async Task DuplicateClass_ReturnsResponseDTO()
        {
            // Arrange
            int classId = 3;

            var expectedResponse = new ResponseDTO
            {
                Result = new Class(),
                IsSuccess = true,
                Message = "Class duplicated successfully"
            };

            //set up the mock IUnitOfWork to return a specific ResponseDTO object when the DuplicatedClass method of the
            //repo is called with the id parameter.
            _unitOfWorkMock.Setup(uow => uow.classRepository.DuplicatedClass(classId))
                .ReturnsAsync(expectedResponse);

            // Act
            //create an instance of the controller call the DuplicateClass method with the classId
            var result = await _controller.DuplicateClass(classId);

            // Assert
            Assert.IsType<ResponseDTO>(result);
            var responseDto = result;
            Assert.Equal(expectedResponse.Result, responseDto.Result);
            Assert.True(responseDto.IsSuccess);
            Assert.Equal(expectedResponse.Message, responseDto.Message);
        }

        [Fact]
        public async Task GetClassDetails_ReturnsResponseDTO()
        {
            // Arrange
            int classDetailId = 3;

            var expectedResponse = new ResponseDTO
            {
                Result = new ClassDetails(),
                IsSuccess = true,
                Message = "Class detail retrieved successfully"
            };

            //set up the mock IUnitOfWork to return a specific ResponseDTO object when the GetById method of the
            //repo is called with the id parameter
            _unitOfWorkMock.Setup(uow => uow.classDetailRepository.GetById(classDetailId))
                .ReturnsAsync(expectedResponse);

            // Act
            var result = await _controller.GetClassDetails(classDetailId);
            //create an instance of the controller call the GetClassDetails method with the id

            // Assert
            //assert that the returned value is of type ResponseDTO and matches the expected response
            Assert.IsType<ResponseDTO>(result);
            var responseDto = result as ResponseDTO;
            Assert.Equal(expectedResponse.Result, responseDto.Result);
            Assert.True(responseDto.IsSuccess);
            Assert.Equal(expectedResponse.Message, responseDto.Message);
        }

        [Fact]
        public async Task GetClass()
        {
            int classId = 3;

            ResponseDTO response = new ResponseDTO { Result = null, IsSuccess = true, Message = "" };

            _unitOfWorkMock.Setup(uow => uow.classRepository.GetById(classId))
                .ReturnsAsync(response);

            // Act
            var result = await _controller.GetClass(classId);
            response = result;

            // Assert
            Assert.IsType<ResponseDTO>(result);
            var responseDto = result;
            Assert.NotNull(responseDto);
            Assert.Equal(response.Result, result.Result);

        }

        [Fact]
        public async Task CreateClass_ReturnsResponseDTO()
        {
            // Arrange
            Class classes = new Class
            {
                // Set properties
                ClassName = "xUnit Test",
                Id = "1",
                LevelId = 1,
                ClassCode = "1",
                FSU = "1",
                Status = "1",
                LocationId = 1,
                NumberOfAttendee = 35,
                DateCreate = DateTime.Now,
                ClassId = 5,
                Syllabus = null,
                Level = null,
                Location = null,
                SyllabusId = 1,
                User = null,
                // thêm field test
            };

            var expectedResponse = new ResponseDTO
            {
                Result = classes,
                IsSuccess = true,
                Message = "Add successfully"
            };

            //set up the mock repo to return the expected result when the Create method is called with
            //the input Class object.
            _unitOfWorkMock.Setup(uow => uow.classRepository.Create(classes))
                       .ReturnsAsync(expectedResponse);
            Assert.True(true);

            //_messageBusMock.Verify(mb => mb.SendMessage(classes, It.IsAny<string>()), Times.Once);
            //verify that the repo property is accessed and the Create method is called with the correct input object.
        }

        [Fact]
        public async Task CreateClassDetail_ReturnsResponseDTO()
        {
            // Arrange
            ClassDetails classDetails = new ClassDetails
            {
                ClassId = 1,
                Id = "1"
            };

            var expectedResponse = new ResponseDTO
            {
                Result = classDetails,
                IsSuccess = true,
                Message = "Add successfully"
            };

            // Set up the mock repository to return the expected result when the Create method is called with the input ClassDetails object.
            _unitOfWorkMock.Setup(uow => uow.classDetailRepository.Create(classDetails))
               .ReturnsAsync(expectedResponse);

            // Act
            var result = await _controller.CreateClassDetail(classDetails);

            // Assert
            // Assert that the returned value is of type ResponseDTO and matches the expected response
            Assert.IsType<ResponseDTO>(result);
            var responseDto = result as ResponseDTO;
            Assert.Equal(expectedResponse.Result, responseDto.Result);
            Assert.True(responseDto.IsSuccess);
            Assert.Equal(expectedResponse.Message, responseDto.Message);
        }

        [Fact]
        public async Task UpdateClass()
        {
            // Arrange
            Class updatedClass = new Class
            {
                ClassId = 3,
                ClassName = "UpdatedClass",
                // Set fields
            };

            var expectedResponse = new ResponseDTO
            {
                Result = updatedClass,
                IsSuccess = true,
                Message = ""
            };

            _unitOfWorkMock.Setup(uow => uow.classRepository.Update(updatedClass))
                .ReturnsAsync(expectedResponse);

            // Act
            var result = await _controller.UpdateClass(updatedClass);

            // Assert
            Assert.IsType<ResponseDTO>(result);
            var responseDto = result;
            Assert.Equal(expectedResponse.Result, responseDto.Result);
            Assert.True(responseDto.IsSuccess);
            Assert.Equal(expectedResponse.Message, responseDto.Message);
        }

        [Fact]
        public async Task UpdateClassDetail_ReturnsResponseDTO()
        {
            // Arrange
            ClassDetails classDetails = new ClassDetails
            {
                // Set the properties of the classDetails 
                ClassId = 3,
            };

            var expectedResponse = new ResponseDTO
            {
                Result = classDetails,
                IsSuccess = true,
                Message = "Class details updated successfully"
            };

            //using the Moq library to mock the IUnitOfWork dependency of the ClassController
            //set up the mock IUnitOfWork to return a ResponseDTO object when the Update method called with the ClassId
            _unitOfWorkMock.Setup(uow => uow.classDetailRepository.Update(classDetails))
                .ReturnsAsync(expectedResponse);

            // Act
            var result = await _controller.UpdateClassDetail(classDetails);
            //create an instance of the TrainingController call the UpdateClassDetail method with the classDetails.

            // Assert
            //assert that the returned value is of type ResponseDTO and matches the expected response
            Assert.IsType<ResponseDTO>(result);
            var responseDto = result as ResponseDTO;
            Assert.Equal(expectedResponse.Result, responseDto.Result);
            Assert.True(responseDto.IsSuccess);
            Assert.Equal(expectedResponse.Message, responseDto.Message);
        }

        [Fact]
        public async Task DeleteClass_ReturnsResponseDTO()
        {
            // Arrange
            var classId = 3;
            var expectedResponse = new ResponseDTO
            {
                Result = "Class deleted successfully",
                IsSuccess = true,
                Message = null
            };

            _unitOfWorkMock.Setup(uow => uow.classRepository.DeleteById(classId))
                .ReturnsAsync(expectedResponse);

            Assert.Equal(expectedResponse.Result, "Class deleted successfully");
        }

        [Fact]
        public async Task DeleteClassDetail_ReturnsResponseDTO()
        {
            // Arrange
            int classDetailId = 3;

            var expectedResponse = new ResponseDTO
            {
                Result = null,
                IsSuccess = true,
                Message = "Class detail deleted successfully"
            };

            //set up the mock IUnitOfWork to return a specific ResponseDTO object when the DeleteById method of the
            //repo is called with the id parameter.
            _unitOfWorkMock.Setup(uow => uow.classDetailRepository.DeleteById(classDetailId))
                .ReturnsAsync(expectedResponse);

            // Act
            //create an instance of the controller call the DeleteClassDetail method with the classDetailId
            var result = await _controller.DeleteClassDetail(classDetailId);

            // Assert
            //assert that the returned value is of type ResponseDTO and matches the expected response.
            Assert.IsType<ResponseDTO>(result);
            var responseDto = result;
            Assert.Equal(expectedResponse.Result, responseDto.Result);
            Assert.True(responseDto.IsSuccess);
            Assert.Equal(expectedResponse.Message, responseDto.Message);
        }

        [Fact]
        public async Task PaginatedClass_ReturnsResponseDTO()
        {
            // Arrange
            var locationIds = new int?[] { 1, 2, 3 };
            var dateFrom = new DateTime(2023, 1, 1);
            var dateTo = new DateTime(2023, 12, 31);
            var attendeeIds = new int?[] { 4, 5, 6 };
            var userId = 1;
            var page = 1;
            var pageSize = 3;

            var expectedResponse = new ResponseDTO
            {
                Result = "Classes retrieved successfully",
                IsSuccess = true,
                Message = null
            };

            //Moq library to mock the IUnitOfWork dependency and set up GetAll method
            _unitOfWorkMock.Setup(uow => uow.classRepository.GetAll(
                page, locationIds, dateFrom, dateTo, attendeeIds, userId, pageSize))
                .ReturnsAsync(expectedResponse);

            // Act
            var result = await _controller.PaginatedClass(
                locationIds, dateFrom, dateTo, attendeeIds, userId, page, pageSize);

            // Assert
            //ensure that the returned result is of type ResponseDTO and matches the expected response.
            Assert.IsType<ResponseDTO>(result);
            var responseDto = result as ResponseDTO;
            Assert.Equal(expectedResponse.Result, responseDto.Result);
            Assert.True(responseDto.IsSuccess);
            Assert.Null(responseDto.Message);

            _unitOfWorkMock.Verify(uow => uow.classRepository.GetAll(
                page, locationIds, dateFrom, dateTo, attendeeIds, userId, pageSize), Times.Once);
        }

    }
}