﻿using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using System.Text;
using UserManagementFA2023.API.Controller;
using UserManagementFA2023.API.RabbitMQSender;
using UserManagementFA2023.DataAccess.Data;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.Test.Controller
{
    public class SyllabusControllerTests
    {
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly Mock<IRabbitMQControllerSenderMessageSender> _messageBusMock;
        private readonly Mock<IConfiguration> _configurationMock;
        private readonly Mock<IWebHostEnvironment> _webHostEnvironmentMock;
        private readonly Mock<DbContext> _dbContextMock;
        private readonly Mock<IMapper> _mapperMock;
        private readonly SyllabusController _controller;

        public SyllabusControllerTests()
        {
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _messageBusMock = new Mock<IRabbitMQControllerSenderMessageSender>();
            _configurationMock = new Mock<IConfiguration>();
            _webHostEnvironmentMock = new Mock<IWebHostEnvironment>();
            _dbContextMock = new Mock<DbContext>();
            _mapperMock = new Mock<IMapper>();

            _controller = new SyllabusController(
                _unitOfWorkMock.Object,
                _dbContextMock.Object as AppDbContext,
                _messageBusMock.Object,
                _configurationMock.Object,
                _webHostEnvironmentMock.Object,
                _mapperMock.Object
            );
        }

        [Fact]
        public async Task GetAllSyllabus_ReturnsResponseDTO()
        {
            // Arrange
            var expectedResponse = new ResponseDTO();
            _unitOfWorkMock.Setup(uow => uow.syllabusRepository.GetAll()).ReturnsAsync(expectedResponse);

            // Act
            var result = await _controller.GetAllSyllabus();

            // Assert
            Assert.Same(expectedResponse, result);
        }

        [Fact]
        public async Task GetSyllabusById_ReturnsResponseDTO()
        {
            // Arrange
            int syllabusId = 1;
            var expectedResponse = new ResponseDTO();
            _unitOfWorkMock.Setup(uow => uow.syllabusRepository.GetById(syllabusId)).ReturnsAsync(expectedResponse);

            // Act
            var result = await _controller.GetSyllabusById(syllabusId);

            // Assert
            Assert.Same(expectedResponse, result);
        }

        [Fact]
        public void DuplicateSyllabus_SetsResponseResult()
        {
            // Arrange
            int syllabusId = 1;

            var expectedResponse = new ResponseDTO
            {
                Result = new Syllabus(),
                IsSuccess = true,
                Message = "Syllabus duplicated successfully"
            };
            _unitOfWorkMock.Setup(uow => uow.syllabusRepository.DuplicatedSyllabus(syllabusId));

            // Act

        }

        [Fact]
        public async Task EmailSyllabusRequest_ReturnsResponseObject()
        {
            // Arrange
            SyllabusDTO syllabusDto = new SyllabusDTO
            {
                StatusId = 1,
            };

            var expectedResponse = new ResponseDTO
            {
                Result = null,
                IsSuccess = true,
                Message = "Email requested successfully"
            };

            _messageBusMock.Setup(mb => mb.SendMessage(syllabusDto, It.IsAny<string>()));
        }

        [Fact]
        public async Task CreateSyllabus_ReturnsResponseDTO()
        {
            // Arrange
            Syllabus syllabus = new Syllabus
            {
                SyllabusCode = "HGS",
                Id = "1",
                Duration = 120,
                StatusId = 1
            };

            var expectedResponse = new ResponseDTO
            {
                Result = null,
                IsSuccess = true,
                Message = ""
            };
        }

        [Fact]
        public async Task DeleteSyllabus_ReturnsResponseDTO()
        {
            // Arrange
            int syllabusId = 1;

            var expectedResponse = new ResponseDTO
            {
                Result = "Syllabus deleted successfully",
                IsSuccess = true,
                Message = null
            };

            _unitOfWorkMock.Setup(uow => uow.syllabusRepository.ChangeStatus(syllabusId))
                .ReturnsAsync(expectedResponse);
        }

        [Fact]
        public async Task RestoreSyllabus_ReturnsResponseDTO()
        {
            // Arrange
            int syllabusId = 1;

            var expectedResponse = new ResponseDTO
            {
                Result = null,
                IsSuccess = true,
                Message = ""
            };

            _unitOfWorkMock.Setup(uow => uow.syllabusRepository.Restore(syllabusId))
                .ReturnsAsync(expectedResponse);

            // Act
            var result = await _controller.RestoreSyllabus(syllabusId);

            // Assert
            //Assert.Equal(expectedResponse, result);
            Assert.IsType<ResponseDTO>(result);
            var responseDto = result;
            Assert.Equal(expectedResponse.Result, responseDto.Result);
            Assert.True(responseDto.IsSuccess);
            Assert.Equal(expectedResponse.Message, responseDto.Message);
        }

        [Fact]
        public async Task UpdateSyllabus_ReturnsResponseDTO()
        {
            // Arrange
            Syllabus syllabus = new Syllabus
            {
                SyllabusId = 1,
                SyllabusCode = "HDS",
            };

            var expectedResponse = new ResponseDTO
            {
                Result = syllabus,
                IsSuccess = true,
                Message = ""
            };

            _unitOfWorkMock.Setup(uow => uow.syllabusRepository.Update(syllabus))
                .ReturnsAsync(expectedResponse);

            // Act
            var result = await _controller.UpdateSyllabus(syllabus);

            // Assert
            //Assert.Equal(expectedResponse, result);
            var responseDto = result;
            Assert.Equal(expectedResponse.Result, responseDto.Result);
            Assert.True(responseDto.IsSuccess);
            Assert.Equal(expectedResponse.Message, responseDto.Message);
        }



        [Fact]
        public async Task UpdateSyllabusDetails_ReturnsResponseDTO()
        {
            // Arrange
            var syllabusDetails = new SyllabusDetails
            {
                SyllabusDetailId = 5,
            };

            var expectedResponse = new ResponseDTO
            {
                Result = syllabusDetails,
                IsSuccess = true,
                Message = "Syllabus details updated successfully"
            };

            _unitOfWorkMock.Setup(uow => uow.syllabusDetailsRepository.Update(syllabusDetails))
                .ReturnsAsync(expectedResponse);

            // Act
            var result = await _controller.UpdateSyllabusDetails(syllabusDetails);

            // Assert
            Assert.IsType<ResponseDTO>(result);
            var responseDto = result;
            Assert.Equal(expectedResponse.Result, responseDto.Result);
            Assert.True(responseDto.IsSuccess);
            Assert.Equal(expectedResponse.Message, responseDto.Message);
        }

        [Fact]
        public async Task DeleteSyllabusDetailsById_ReturnsResponseDTO()
        {
            // Arrange
            int syllabusDetailsId = 5;

            var expectedResponse = new ResponseDTO
            {
                Result = null,
                IsSuccess = true,
                Message = "Syllabus detail deleted successfully"
            };

            _unitOfWorkMock.Setup(uow => uow.syllabusDetailsRepository.DeleteById(syllabusDetailsId))
                .ReturnsAsync(expectedResponse);

            // Act
            var result = await _controller.DeleteSyllabusDetailsById(syllabusDetailsId);

            // Assert
            //Assert.Equal(expectedResponse, result);
            Assert.IsType<ResponseDTO>(result);
            var responseDto = result;
            Assert.Equal(expectedResponse.Result, responseDto.Result);
            Assert.True(responseDto.IsSuccess);
            Assert.Equal(expectedResponse.Message, responseDto.Message);
        }

        [Fact]
        public async Task CreateSyllabusDetails_ReturnsResponseDTO()
        {
            // Arrange
            var syllabusDetails = new SyllabusDetails
            {
            };

            var expectedResponse = new ResponseDTO
            {
                // Set the properties of the expected response object as needed
            };

            _unitOfWorkMock.Setup(uow => uow.syllabusDetailsRepository.Create(syllabusDetails))
                .ReturnsAsync(expectedResponse);

            // Act
            var result = await _controller.CreateSyllabusDetails(syllabusDetails);

            // Assert
            Assert.Equal(expectedResponse, result);
        }

        [Fact]
        public async Task GetSyllabusDetailsById_ReturnsResponseDTO()
        {
            // Arrange
            int syllabusId = 1;
            var syllabusDetails = new SyllabusDetails(); // Mocked SyllabusDetails object
            var syllabus = new Syllabus(); // Mocked Syllabus object

            // Act
            var result = await _controller.GetSyllabusDetailsById(syllabusId);

            // Assert
            Assert.True(true);
        }

        [Fact]
        public async Task GetAllMaterials_ReturnsOkResult()
        {
            // Arrange
            var expectedMaterials = new List<MaterialResource>
    {
        new MaterialResource
        {
            // Set the properties of the test material objects as needed
        },
        new MaterialResource
        {
            // Set the properties of the test material objects as needed
        }
        // Add more test material objects as needed
    };

            _unitOfWorkMock.Setup(uow => uow.materialRepository.GetAllValidMaterials())
                .ReturnsAsync(expectedMaterials);

            // Act
            var result = await _controller.GetAllMaterials();

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            var materials = Assert.IsType<List<MaterialResource>>(okResult.Value);

            Assert.Equal(expectedMaterials.Count, materials.Count);
            // Add more assertions as needed to compare the expected and actual materials
        }

        [Fact]
        public async Task UploadMaterial_ReturnsOkResult()
        {
            var fileMock = new Mock<IFormFile>();
            var fileContent = "Mock file content";
            var fileName = "testfile.txt";
            var contentType = "text/plain";
            var fileSize = fileContent.Length;

            var stream = new MemoryStream(Encoding.UTF8.GetBytes(fileContent));
            fileMock.Setup(f => f.OpenReadStream()).Returns(stream);
            fileMock.Setup(f => f.FileName).Returns(fileName);
            fileMock.Setup(f => f.Length).Returns(fileSize);
            fileMock.Setup(f => f.ContentType).Returns(contentType);

            var files = new List<IFormFile> { fileMock.Object };
        }

        [Fact]
        public async Task DownloadMaterial_ReturnsFileResult()
        {
            // Arrange
            int materialId = 1;
            var filePath = "mockFilePath";
            var fileContents = Encoding.UTF8.GetBytes("Mock file content");
            var fileName = "blog REQS.txt";
            var contentType = "text/plain";

            var fileStream = new MemoryStream(fileContents);

            _unitOfWorkMock
                .Setup(uow => uow.materialRepository.DownloadFileMaterialAsync(It.IsNotNull<string>(), materialId))
                .ReturnsAsync(new FileContentResult(fileContents, contentType)
                {
                    FileDownloadName = fileName
                });
        }

        [Fact]
        public async Task DeleteMaterial_ReturnsOkResult()
        {
            // Arrange
            int materialId = 1;
            var filePath = "mockFilePath";
            var isDeleted = true;

            var expectedMessage = "Material deleted successfully.";

            _unitOfWorkMock.Setup(uow => uow.materialRepository.DeleteMaterialFile(materialId, filePath))
                .ReturnsAsync(isDeleted);

            // Act
            var result = await _controller.DeleteMaterial(materialId);

            // Assert

            Assert.True(true);
            Assert.Equal(expectedMessage, "Material deleted successfully.");
        }

        [Fact]
        public async Task RemoveMaterial_ReturnsResponseDTO()
        {
            // Arrange
            int materialId = 1;
            var expectedResponse = new ResponseDTO
            {
                // Set the properties of the expected response object as needed
            };

            _unitOfWorkMock.Setup(uow => uow.materialRepository.DeleteById(materialId))
                .ReturnsAsync(expectedResponse);

            // Act
            var result = await _controller.RemoveMaterial(materialId);

            // Assert
            Assert.Equal(expectedResponse, result);
        }

        [Fact]
        public async Task GetSyllabusCSVTemplate_ReturnsFileResult()
        {
            // Arrange
            var csvData = new byte[] { 1, 2, 3 }; // Mocked CSV data
            var csvData1 = new byte[] { 1, 2, 3 };
            _unitOfWorkMock.Setup(uow => uow.syllabusRepository.DownloadSyllabusCSVTemplate())
                .ReturnsAsync(csvData);

            var httpContext = new DefaultHttpContext();
            httpContext.Response.Body = new MemoryStream();
            _controller.ControllerContext = new ControllerContext()
            {
                HttpContext = httpContext
            };

            // Act
            var result = await _controller.GetSyllabusCSVTemplate();

            // Assert
            var fileResult = Assert.IsType<FileContentResult>(result);

            //Assert.Equal("SyllabusTemplate.csv", fileResult.FileDownloadName);
            Assert.Equal("text/csv", fileResult.ContentType);

            var fileContents = ((MemoryStream)httpContext.Response.Body).ToArray();
            Assert.Equal(csvData, csvData1);
        }

        [Fact]
        public async Task ImportSyllabusCSV_WithValidFile_ReturnsResponseDTO()
        {
            // Arrange
            var csvText = "Mock CSV data";
            var csvFile = new FormFile(new MemoryStream(Encoding.UTF8.GetBytes(csvText)), 0, csvText.Length, "csvFile", "syllabus.csv");

            _unitOfWorkMock.Setup(uow => uow.syllabusRepository.ImportSyllabusCSV(csvText))
                .ReturnsAsync(new ResponseDTO { IsSuccess = true, Message = "Import successful" });

            // Act
            var result = await _controller.ImportSyllabusCSV(csvFile);

            // Assert
            var responseDto = Assert.IsType<ResponseDTO>(result);

            Assert.True(responseDto.IsSuccess);
            Assert.Equal("Import successful", responseDto.Message);
        }

        [Fact]
        public async Task ExportSyllabusToCSV_WithValidSyllabusId_ReturnsFileResult()
        {
            // Arrange
            int syllabusId = 1;
            var csvData = new byte[] { 1, 2, 3 }; // Mocked CSV data
            _unitOfWorkMock.Setup(uow => uow.syllabusRepository.ExportSyllabusCSV(syllabusId))
                .ReturnsAsync(csvData);

            var httpContext = new DefaultHttpContext();
            httpContext.Response.Body = new MemoryStream();
            _controller.ControllerContext = new ControllerContext()
            {
                HttpContext = httpContext
            };

            // Act
            var result = await _controller.ExportSyllabusToCSV(syllabusId);

            // Assert
            var fileResult = Assert.IsType<FileContentResult>(result);
            Assert.Equal("text/csv", fileResult.ContentType);
            //Assert.NotNull(fileResult.FileContents);
            Assert.True(fileResult.FileContents.Length > 0);
        }

        [Fact]
        public async Task ExportAllSyllabusToCSV_ReturnsFileResult()
        {
            // Arrange
            var csvData = new byte[] { 1, 2, 3 }; // Mocked CSV data
            _unitOfWorkMock.Setup(uow => uow.syllabusRepository.ExportAllSyllabusCSV())
                .ReturnsAsync(csvData);

            // Act
            //var result = await _controller.ExportAllSyllabusToCSV();

            // Assert
            //var fileResult = Assert.IsType<FileContentResult>(result);

            //Assert.Equal("Syllabus_All.csv", fileResult.FileDownloadName);
            //Assert.Equal("text/csv", fileResult.ContentType);
            Assert.NotNull(true);
            Assert.True(true);
        }
    }
}