﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using UserManagementFA2023.API.Controller;
using UserManagementFA2023.API.RabbitMQSender;
using UserManagementFA2023.DataAccess.Data;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.Test.Controller
{
    public class TrainingProgramControllerTests
    {
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly Mock<IRabbitMQControllerSenderMessageSender> _messageBusMock;
        private readonly Mock<IConfiguration> _configurationMock;
        private readonly DbContextOptions<AppDbContext> _dbContextOptions;
        private readonly AppDbContext _dbContext;
        private readonly TrainingController _controller;

        public TrainingProgramControllerTests()
        {
            _dbContextOptions = new DbContextOptionsBuilder<AppDbContext>()
            .Options;
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _messageBusMock = new Mock<IRabbitMQControllerSenderMessageSender>();
            _configurationMock = new Mock<IConfiguration>();
            _dbContext = new AppDbContext(_dbContextOptions);
            _controller = new TrainingController(_unitOfWorkMock.Object, _messageBusMock.Object, _configurationMock.Object, _dbContext);
        }

        [Fact]
        public async Task CreateTrainingProgram_ReturnsResponseDTO()
        {
            // Arrange
            var inputObj = new TrainningProgram { };

            var expectedResponse = new ResponseDTO
            {
                Result = "Training program created successfully",
                IsSuccess = true,
                Message = null
            };

            _unitOfWorkMock.Setup(uow => uow.trainingProgramRepository.Create(inputObj))
                .ReturnsAsync(expectedResponse);
        }


        [Fact]
        public async Task ActiveTrainingProgram_ReturnsOkResult()
        {
            // Arrange
            int programId = 4;

            var expectedResponse = new ResponseDTO
            {
                Result = null,
                IsSuccess = true,
                Message = "Active Training successfully."
            };

            _unitOfWorkMock.Setup(uow => uow.trainingProgramRepository.ActiveProgram(programId))
                .Returns(valueFunction: (Delegate)expectedResponse.Result);
        }

        [Fact]
        public async Task DeactivateTrainingProgram_ReturnsOkResult()
        {
            // Arrange
            int programId = 4;

            var expectedResponse = new ResponseDTO
            {
                Result = null,
                IsSuccess = true,
                Message = ""
            };

            _unitOfWorkMock.Setup(uow => uow.trainingProgramRepository.DeactiveProgram(programId))
                .Returns(valueFunction: (Delegate)expectedResponse.Result);
        }

        [Fact]
        public async Task SearchTrainingPrograms_ReturnsOkResult()
        {
            // Arrange
            string keyword = "Pro";

            var expectedPrograms = new List<TrainningProgram>
            {
                new TrainningProgram
                {
                    TrainningProgramId = 3,
                    TrainningProgramName = "Program1",
                    Id = "1",
                    User = null,
                    Duration = "180",
                    StatusId = 1,
                    Status = null
                },
                new TrainningProgram
                {

                    TrainningProgramId = 4,
                    TrainningProgramName = "Program2",
                    Id = "1",
                    User = null,
                    Duration = "200",
                    StatusId = 2,
                    Status = null
                }
            };

            _unitOfWorkMock.Setup(uow => uow.trainingProgramRepository.SearchTrainingPrograms(keyword))
                .ReturnsAsync(expectedPrograms);

            // Act
            var result = await _controller.SearchTrainingPrograms(keyword);

            // Assert
            Assert.IsType<OkObjectResult>(result);
            var okResult = result as OkObjectResult;
        }

        [Fact]
        public async Task RestoreTrainingProgram_ReturnsResponseDTO()
        {
            // Arrange
            int programId = 1;

            var expectedResponse = new ResponseDTO
            {
                Result = null,
                IsSuccess = true,
                Message = ""
            };

            _unitOfWorkMock.Setup(uow => uow.trainingProgramRepository.Restore(programId))
                .Returns((Delegate)expectedResponse.Result);

            // Act
            var result = _controller.RestoreTrainingProgram(programId);

            // Assert
            Assert.IsType<ResponseDTO>(result);
            var responseDto = result as ResponseDTO;
            Assert.Equal(expectedResponse.Result, responseDto.Result);
            Assert.True(responseDto.IsSuccess);
            Assert.Equal(expectedResponse.Message, responseDto.Message);
        }

        // Arrange

        // Act

        // Assert

        [Fact]
        public async Task GetAllTrainingProgram_ReturnsResponseDTO()
        {
            // Arrange
            var expectedResponse = new ResponseDTO
            {
                Result = null,
                IsSuccess = true,
                Message = ""
            };
            _unitOfWorkMock.Setup(uow => uow.trainingProgramRepository.GetAll())
                .ReturnsAsync(expectedResponse);

            // Act
            var result = await _controller.GetAllTrainingProgram();

            // Assert
            var responseDto = Assert.IsType<ResponseDTO>(result);
        }

        [Fact]
        public async Task GetTrainingProgramDetails_ReturnsResponseDTO()
        {
            // Arrange
            int programId = 5;
            var expectedResponse = new ResponseDTO
            {
                Result = new TrainningProgram { },
                IsSuccess = true,
                Message = ""
            };

            _unitOfWorkMock.Setup(uow => uow.trainingProgramRepository.GetById(programId))
                .ReturnsAsync(expectedResponse);

            // Act
            var result = await _controller.GetTrainingProgram(programId);

            // Assert
            var responseDto = Assert.IsType<ResponseDTO>(result);

            Assert.Equal(expectedResponse.Result, responseDto.Result);
            Assert.Equal(expectedResponse.IsSuccess, responseDto.IsSuccess);
            Assert.Equal(expectedResponse.Message, responseDto.Message);
        }

        [Fact]
        public async Task UpdateTrainingProgram_ReturnsResponseDTO()
        {
            // Arrange
            var trainingProgram = new TrainningProgram { };
            var expectedResponse = new ResponseDTO
            {
                Result = trainingProgram,
                IsSuccess = true,
                Message = ""
            };

            _unitOfWorkMock.Setup(uow => uow.trainingProgramRepository.Update(trainingProgram))
                .ReturnsAsync(expectedResponse);

            // Act
            var result = await _controller.UpdateTrainingProgram(trainingProgram);

            // Assert
            var responseDto = Assert.IsType<ResponseDTO>(result);

            Assert.Equal(expectedResponse.Result, responseDto.Result);
            Assert.Equal(expectedResponse.IsSuccess, responseDto.IsSuccess);
            Assert.Equal(expectedResponse.Message, responseDto.Message);
        }

        [Fact]
        public async Task DeleteTrainingProgram_ReturnsResponseDTO()
        {
            // Arrange
            int programId = 1;
            var expectedResponse = new ResponseDTO
            {
                Result = true,
                IsSuccess = true,
                Message = ""
            };
            _unitOfWorkMock.Setup(uow => uow.trainingProgramRepository.DeleteById(programId))
    .ReturnsAsync(expectedResponse);
            // Act
            var result = await _controller.DeleteTrainingProgram(programId);

            // Assert
            var responseDto = Assert.IsType<ResponseDTO>(result);
        }
    }

}
