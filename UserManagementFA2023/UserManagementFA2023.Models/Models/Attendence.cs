﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models
{
    public class Attendence
    {
        [Key, Column(Order =1)]
        public int UnitId { get; set; }
        [ForeignKey("UnitId")]
        public Unit? Unit { get; set; }  
        public int? ClassId { get; set; }
        [ForeignKey("ClassId")]
        public Class? Class { get; set; }    
        public bool IsAttended { get; set; }   

    }
}
