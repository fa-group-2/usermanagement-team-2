﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models
{
    public class ClassDetails
    {
        [Key, Column(Order =1)] 
        public int ClassId { get; set; }
        [ForeignKey("ClassId")]
        public Class? Class { get; set; }
        [Key, Column(Order = 2)]
        public string? Id { get; set; }
        [ForeignKey("Id")]
        public ApplicationUser? User { get; set; }
    }
}
