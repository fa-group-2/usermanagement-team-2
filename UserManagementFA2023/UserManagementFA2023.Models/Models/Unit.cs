﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models
{
    public class Unit
    {
        [Key]
        public int UnitId { get; set; }   
        public string? UnitName { get; set; }        
        public int? MaterialId { get; set; }
        [ForeignKey("MaterialId")]
        public MaterialResource? MaterialResource { get; set; }
        public ICollection<Lesson>? lessons { get; set; }
        public int TimeLineId { get; set; }
        [ForeignKey("TimeLineId")]
        public Timeline Timeline { get; set; }
    }
}
