﻿using System.ComponentModel.DataAnnotations;

namespace UserManagementFA2023.Models.Models
{
    public class Status
    {
        [Key]
        public int StatusId { get; set; } 
        public bool? IsActive { get; set; }  
     
    }
}
