﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagementFA2023.Models.Models
{
    public class Permission
    {
        public Permission() { }
        public Permission(long id, string roleId ,bool create, bool view, bool fullAccess, bool accessDenied, bool modify)
        {
            Id = id;    
            RoleId = roleId;    
            Create = create;    
            View = view;    
            FullAccess = fullAccess;    
            AccessDenied = accessDenied; 
            Modify = modify;    
        }
        [Key]
        public long Id { get; set; }
        [Required]
        public string RoleId { get; set; }

        [StringLength(128)]
        [Required]
        public string FunctionId { get; set; }
        [ForeignKey("FunctionId")]
        public virtual Functions Function { get; set; }
        public bool Create { set; get; }
        public bool View { set; get; }

        public bool FullAccess { set; get; }
        public bool AccessDenied { set; get; }
        public bool Modify { set; get; }

    }
}