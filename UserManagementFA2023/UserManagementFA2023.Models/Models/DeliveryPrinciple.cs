﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagementFA2023.Models.Models
{
    public class DeliveryPrinciple
    {
        [Key]
        public int PrincipleId { get; set; }
        public string? Training { get; set; }    
        public string? ReTest { get; set; }    
        public string? Marking { get; set; }    
        public string? Waiver { get; set; }    
        public string? Others { get; set; }   
    }
}
