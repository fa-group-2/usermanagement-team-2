﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models
{
    public class Syllabus
    {
        [Key]
        public int SyllabusId { get; set; }
        public string? SyllabusTittle { get; set; }
        public string? SyllabusCode { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string? Id { get; set; }
        [ForeignKey("Id")]
        public ApplicationUser? User { get; set; }
        public int? Duration { get; set; }   
        public int? StatusId { get; set; }
        [ForeignKey("StatusId")]
        public Status? Status { get; set; }
        public int? PrincipleId { get; set; }
        [ForeignKey("PrincipleId")]
        public DeliveryPrinciple? DeliveryPrinciple { get; set; }
        public int? AssessId { get; set; }
        [ForeignKey("AssessId")]
        public AssessmentScheme? AssessmentScheme { get; set; }
    }
}
