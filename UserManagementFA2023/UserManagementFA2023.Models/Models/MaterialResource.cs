﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagementFA2023.Models.Models
{
    public class MaterialResource
    {
        [Key]
        public int MaterialId { get; set; }
        public string? FileName { get; set; }
        public string? ContentType { get; set; }
        public long? FileSize { get; set; }
        public DateTime? UploadDate { get; set; }
        public bool? Status { get; set; }
    }
}
