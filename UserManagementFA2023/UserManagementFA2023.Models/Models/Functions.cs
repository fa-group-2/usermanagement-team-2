﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagementFA2023.Models.Models
{
    public class Functions
    {
        public Functions() { }
        public Functions(string iconCss, string id, string name, string parentId, int sortOrder, int status, string uRL)
        {
            IconCss = iconCss;
            Id = id;
            Name = name;
            ParentId = parentId;
            SortOrder = sortOrder;
            Status = status;
            URL = uRL;
        }

        [Key]
        [StringLength(128)]
        public string Id { get; set; }
        public string IconCss { get; set; }
        public string Name { get; set; }
        public string ParentId { get; set; }
        public int SortOrder { get; set; }
        public int Status { get; set; }
        public string URL { get; set; }
    }
}
