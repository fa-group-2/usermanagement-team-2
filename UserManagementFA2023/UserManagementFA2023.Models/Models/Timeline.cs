﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models
{
    public class Timeline
    {
        [Key]
        public int TimeLineId { get; set; } 
        public int? SyllabusDetailId { get; set; }
        [ForeignKey("SyllabusDetailId")]
        public SyllabusDetails? SyllabusDetails { get; set;}
        public string? Description { get; set; } 
        public ICollection<Unit>? Units { get; set; }
        public int? MatertialTypeId { get; set; }
        [ForeignKey("MatertialTypeId")]    
        public MaterialType? MaterialType { get; set; }  
        
    }
}
