﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagementFA2023.Models.Models.DTO
{
    public class AssessmentSchemeDTO
    {
        public int? AssessId { get; set; }
        public int? Quiz { get; set; }
        public int? Assignment { get; set; }
        public int? Final { get; set; }
        public int? FinalTheory { get; set; }
        public int? FinalPractice { get; set; }
        public int? GPA { get; set; }
    }
}
