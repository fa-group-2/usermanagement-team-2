﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models.DTO
{
    public class ClassDetailsDTO
    {
        public int ClassId { get; set; }
        public Class? Class { get; set; }
        public string? Id { get; set; }
        public ApplicationUser? User { get; set; }
    }
}
