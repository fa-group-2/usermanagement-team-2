﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models.DTO
{
    public class UnitDTO
    {
        public int UnitId { get; set; }   
        public string? UnitName { get; set; }        
        public int? MaterialId { get; set; }
        public MaterialResourceDTO? MaterialResource { get; set; }
        public int? TimeLineId { get; set; }
        public Timeline? Timeline { get; set; }
    }
}
