﻿using System.ComponentModel.DataAnnotations;

namespace UserManagementFA2023.Models.Models
{
    public class LocationDTO
    {
        public int LocationId { get; set; } 
        public string? LocationName { get; set; }    
        public string? District { get; set; }    
        public string? Province { get; set; }    
    }
}
