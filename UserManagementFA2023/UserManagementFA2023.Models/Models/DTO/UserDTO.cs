﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models.DTO    
{
    public class UserDTO
    {
        public string? Id { get; set; }  
        public string? Name { get; set; }    
        public string? Email { get; set; }   
        public string? PhoneNumber { get; set; }
        public string? Token { get; set; }
        public string? ImageUrl { get; set; }
        public string? ImageLocalPath { get; set; }
        public IFormFile? Image { get; set; }
    }
}
