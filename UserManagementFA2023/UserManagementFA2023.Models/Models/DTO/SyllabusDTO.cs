﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagementFA2023.Models.Models.DTO
{
    public class SyllabusDTO
    {
        public int SyllabusId { get; set; }
        public string? SyllabusTittle { get; set; }
        public string? SyllabusCode { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string? Id { get; set; }
        public UserDTO? User { get; set; }
        public int? Duration { get; set; }
        public int? StatusId { get; set; }
        public StatusDTO? Status { get; set; }
    }
}
