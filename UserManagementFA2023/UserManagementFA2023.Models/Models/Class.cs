﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models
{
	public class Class
	{
		[Key]
		public int ClassId { get; set; }
		public string? ClassName { get; set; }
		public string? ClassCode { get; set; }
		public DateTime? DateCreate { get; set; }
		[ForeignKey("LocationId")]
		public int? LocationId { get; set; }
		public Location? Location { get; set; }
		public string? FSU { get; set; }
		public string? Status { get; set; }
		public string? Id { get; set; }
		[ForeignKey("Id ")]
		public ApplicationUser? User { get; set; }
		public int? NumberOfAttendee { get; set; }
		public int LevelId { get; set; }
		[ForeignKey("LevelId")]
		public Level? Level { get; set; }
		public DateTime? DateFrom { get; set; }
		public DateTime? DateTo { get; set; }
		public int? SyllabusId { get; set; }
		[ForeignKey("SyllabusId")]
		public Syllabus? Syllabus { get; set; }
		public int TimeStart { get; set; }
		public int TimeEnd { get; set; }
	}
}
