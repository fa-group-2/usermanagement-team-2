﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models
{
    public class SyllabusDetails
    {
		[Key]
		public int SyllabusDetailId { get; set; }
		public int SyllabusId { get; private set; }
		[ForeignKey("SyllabusId")]
		public Syllabus? Syllabus { get; set; }
		public DateTime? LastModifyDate { get; set; }
		public string? Description { get; set; }
		public string? RequirementDescription { get; set; }
		public int? LevelId { get; set; }
		[ForeignKey("LevelId")]
		public Level? Level { get; set; }
		public int? AtendeeNumber { get; set; }
	}
}
