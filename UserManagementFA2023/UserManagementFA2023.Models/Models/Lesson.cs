﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models
{
    public class Lesson
    {
        [Key]
        public int LessonId { get; set; } 
        public string? LessonName { get; set; }  
        public string? OutputStandard { get; set; } 
        public string? Method { get; set; }    
        public int? TraningTime { get; set; }    
        public int? UnitId { get; set; }
        [ForeignKey("UnitId")]
        public Unit? Unit { get; set; }  
    }
}
