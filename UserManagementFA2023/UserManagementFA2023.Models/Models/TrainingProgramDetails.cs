﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagementFA2023.Models.Models
{
    public class TrainingProgramDetails
    {
        [Key]
        public int TrainingProgramDetailsId { get; set; } 
        public int TrainningProgramId { get; set;}
        [ForeignKey("TrainningProgramId")]
        public TrainningProgram? TrainningProgram { get; set; }  
        public string GeneralInformation { get; set; }  
        public int SyllabusId { get; set; }
        [ForeignKey("SyllabusId")]
        public Syllabus? Syllabus { get; set; }  
        public int ClassId { get; set; }
        [ForeignKey("ClassId")]
        public Class? Class { get; set; }
    }
}
