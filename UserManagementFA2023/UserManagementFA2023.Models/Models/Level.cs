﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.Models
{
    public class Level
    {
        [Key]
        public int LevelId { get; set; }
        public string? LevelName { get; set; }
    }
}
