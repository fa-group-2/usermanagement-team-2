﻿namespace UserManagementFA2023.Utility
{
    public class SD
    {
        public static string ControllerAPI { get; set; }

        public const string RoleSuperAdmin = "SUPER ADMIN";
        public const string RoleAdmin = "ADMIN";
        public const string RoleTrainer = "TRAINER";
        public const string TokenCookie = "JwtTokent";
        public enum ApiType
        {
            GET,
            POST,
            PUT,
            DELETE
        }

        public const string Status_Active = "Active";
        public const string Status_Deactive = "Deactive";

        public enum ContentType
        {
            Json,
            MultipartFormData,
        }
    }
}
