﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using UserManagementFA2023.Models.DTO;
using UserManagementFA2023.Service;
using UserManagementFA2023.Service.IService;

namespace UserManagementFA2023.Controllers
{
    public class TrainingController : Controller
    {
        private readonly ITrainingService _trainingService;

        public TrainingController(ITrainingService trainingService)
        {
            _trainingService = trainingService;
        }
        public async Task<IActionResult> TrainingIndex()
        {
            List<TrainingProgramDTO> trainingProgram = new List<TrainingProgramDTO>();
            try
            {
                ResponseDTO? response = await _trainingService.GetAllTrainingProgram();
                if (response != null && response.IsSuccess)
                {
                    trainingProgram = JsonConvert.DeserializeObject<List<TrainingProgramDTO>>(Convert.ToString(response.Result));
                }
                else
                {
                    TempData["error"] = response?.Message;
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = "Đã xảy ra lỗi: " + ex.Message;
                Console.WriteLine("Lỗi: " + ex.ToString());
            }

            return View(trainingProgram);
        }


    }
}
