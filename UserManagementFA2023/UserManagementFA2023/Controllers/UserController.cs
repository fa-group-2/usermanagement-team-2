﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using Newtonsoft.Json;
using Stripe;
using UserManagementFA2023.Models.DTO;
using UserManagementFA2023.Service.IService;

namespace UserManagementFA2023.Controllers
{
    public class UserController : Controller
    {
        public readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService; 
        }
        public async Task<IActionResult> UserIndex(string name)
        {
           ResponseDTO? response = await _userService.GetUserByName(name);  
            if (response != null && response.IsSuccess)
            {
                UserDTO userDTO = JsonConvert.DeserializeObject<UserDTO>(Convert.ToString(response.Result));
                return View(userDTO);
            }
            else
            {
                TempData["error"] = response?.Message;
            }
            return NotFound();
        }
        public async Task<IActionResult> UserEdit(string name) 
        {
            ResponseDTO? response = await _userService.GetUserByName(name);
    
            if (response != null && response.IsSuccess)
            {
                UserDTO? model = JsonConvert.DeserializeObject<UserDTO>(Convert.ToString(response.Result));
                return View(model);
            }
            else
            {
                TempData["error"] = response?.Message;
            }
            return NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> UserEdit(UserDTO user)
        {
            if (ModelState.IsValid)
            {
                ResponseDTO? response = await _userService.UpdateUser(user);
                if (response != null && response.IsSuccess)
                {
                    TempData["success"] = "Product updated successfully";
                    return RedirectToAction(nameof(UserIndex));
                }
                else
                {
                    TempData["error"] = response?.Message;      
                }
            }
             return View(user);
        }   
    }
}
