﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using UserManagementFA2023.Models.DTO;
using UserManagementFA2023.Service;
using UserManagementFA2023.Service.IService;

namespace UserManagementFA2023.Controllers
{
	public class ClassController : Controller
	{
		private readonly IClassService _classService;
		private readonly ILocationService _locationService;
		private	readonly ILevelService _levelService;
        private readonly IUserService _userService;
        public ClassController(IClassService classService,ILocationService locationService,ILevelService levelService, IUserService userService)
		{
			_classService = classService;
			_locationService = locationService;
			_levelService = levelService;
			_userService = userService;
		}

		public async Task<IActionResult> ClassIndex(int page = 1 , int pageSize = 3)
		{
            List<ClassDTO>? listClass ;
			List<LocationDTO>? listLocation;
			List<LevelDTO>? listLevel;
			List<UserDTO>? listUser;
			PageDTO? pageDTO = new PageDTO();
			ResponseDTO? responseClass = await _classService.GetClass(page, pageSize);
			ResponseDTO? responseLocation = await _locationService.GetAllLocation();
			ResponseDTO? responseLevel = await _levelService.GetAllLevel();
            ResponseDTO? responseUser = await _userService.GetAllUser();
            ClassListViewDTO classListViewDTO = new ClassListViewDTO();


            if (responseClass != null && responseClass.IsSuccess && responseLocation != null && responseLocation.IsSuccess
                && responseLevel != null && responseLevel.IsSuccess && responseUser != null && responseUser.IsSuccess)
			{
                listClass = JsonConvert.DeserializeObject<List<ClassDTO>>(Convert.ToString(responseClass.Result));
                listLocation = JsonConvert.DeserializeObject<List<LocationDTO>>(Convert.ToString(responseLocation.Result));
				listLevel = JsonConvert.DeserializeObject<List<LevelDTO>>(Convert.ToString(responseLevel.Result));
				listUser = JsonConvert.DeserializeObject<List<UserDTO>>(Convert.ToString(responseUser.Result));
				var stringPage = Convert.ToString(responseClass.Message);
                string[] parts = stringPage.Split(':');
                if (parts.Length == 2)
                {
                    string totalPageString = parts[1].Trim(); 
					
                    if (int.TryParse(totalPageString, out int totalPage))
                    {
                        pageDTO.TotalPage = totalPage;
						pageDTO.CurrentPageNumber = page;
						pageDTO.PageSize = pageSize;
					}
				}
                if (!listClass.IsNullOrEmpty() && !listLocation.IsNullOrEmpty() &&
					!listLevel.IsNullOrEmpty() && !listUser.IsNullOrEmpty()
					)  
				{
					classListViewDTO.classDTOs = listClass;
                    classListViewDTO.locationDTOs = listLocation;
					classListViewDTO.levelDTOs = listLevel;
					classListViewDTO.userDTOs = listUser;
					classListViewDTO.pageDTO = pageDTO;
                }
				
            }
			else
			{
				/*TempData["error"] = response?.Message;*/
			}

			

			return View(classListViewDTO);
		}
        public async Task<IActionResult> CreateClassDetails()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateClassDetails(ClassDetailsDTO c)
        {
            if (ModelState.IsValid)
            {
                ResponseDTO? response = await _classService.CreateClass(c);

                if (response != null && response.IsSuccess)
                {
                    TempData["success"] = "Syllabus created successfully";
                    return RedirectToAction(nameof(CreateClassDetails));
                }
                else
                {
                    TempData["error"] = response?.Message;
                }
            }
            return View();
        }
    }
}
