﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using UserManagementFA2023.Models.DTO;
using UserManagementFA2023.Service;
using UserManagementFA2023.Service.IService;

namespace UserManagementFA2023.Controllers
{
    public class SyllabusController : Controller
    {
        private readonly ISyllabusService _syllabusService;
        private readonly IStatusService _statusService;
        public SyllabusController(ISyllabusService syllabusService, IStatusService statusService)
        {
            _syllabusService = syllabusService;
            _statusService = statusService;
        }

        public async Task<IActionResult> SyllabusIndex()
        {
            List<SyllabusDTO> listSyllabus;
            List<StatusDTO> listStatus;

            ResponseDTO? responseSyllabus = await _syllabusService.GetAllSyllabus();
            ResponseDTO? responseStatus = await _statusService.GetAllStatus();

            CombinedSyllabusModelDTO syllabusListViewDTO = new CombinedSyllabusModelDTO();

            if (responseSyllabus != null && responseSyllabus.IsSuccess && responseStatus != null && responseStatus.IsSuccess)
            {
                listSyllabus = JsonConvert.DeserializeObject<List<SyllabusDTO>>(Convert.ToString(responseSyllabus.Result));
                listStatus = JsonConvert.DeserializeObject<List<StatusDTO>>(Convert.ToString(responseStatus.Result));
                if (!listSyllabus.IsNullOrEmpty() && !listStatus.IsNullOrEmpty())
                {
                    syllabusListViewDTO.syllabusDTOs = listSyllabus;
                    syllabusListViewDTO.statusDTOs = listStatus;
                }
                else
                {
                    TempData["error"] = responseSyllabus?.Message;
                }

            }
            return View(syllabusListViewDTO);
        }
        [HttpGet]
        public async Task<IActionResult> CreateSyllabusDetails()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateSyllabusDetails(SyllabusDetailsDTO sy)
        {
            if (ModelState.IsValid)
            {
                ResponseDTO? response = await _syllabusService.CreateSyllabus(sy);

                if (response != null && response.IsSuccess)
                {
                    TempData["success"] = "Syllabus created successfully";
                    return RedirectToAction(nameof(CreateSyllabusDetails));
                }
                else
                {
                    TempData["error"] = response?.Message;
                }
            }
            return View();
        }
        public async Task<IActionResult> SyllabusDetail(int syllabusId)
        {
            SyllabusDetailsDTO syllabusDetail = new SyllabusDetailsDTO();
            try
            {
                ResponseDTO? response = await _syllabusService.GetSyllabusDetail(syllabusId);
                if (response != null && response.IsSuccess)
                {
                    syllabusDetail = JsonConvert.DeserializeObject<SyllabusDetailsDTO>(Convert.ToString(response.Result));
                }
                else
                {
                    TempData["error"] = response?.Message;
                }
            }
            catch (Exception ex)
            {
                TempData["error"] = "Đã xảy ra lỗi: " + ex.Message;
                Console.WriteLine("Lỗi: " + ex.ToString());
            }

            return View(syllabusDetail);
        }
    }
}
