﻿function openNav() {
    document.getElementById("menu-icon").style.display = "none";
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    document.getElementById("mainnav").style.marginLeft = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
    document.getElementById("mainnav").style.marginLeft = "0";
    document.getElementById("menu-icon").style.display = "block";
}