﻿async function downloadSyllabusTemplate() {
    try {
        const response = await fetch('https://localhost:7000/api/syllabus/CSV/GetSyllabusTemplate');
        if (response.ok) {
            const blob = await response.blob();
            const url = window.URL.createObjectURL(blob);

            const link = document.createElement('a');
            link.href = url;
            link.download = 'SyllabusTemplate.csv';
            document.body.appendChild(link);
            link.click();

            setTimeout(() => {
                document.body.removeChild(link);
                window.URL.revokeObjectURL(url);
            }, 100); 
        } else {
            console.error('Failed to fetch Syllabus template:', response.statusText);
        }
    } catch (error) {
        console.error('Error:', error);
    }
}

document.getElementById('downloadLink').addEventListener('click', () => {
    downloadSyllabusTemplate();
});