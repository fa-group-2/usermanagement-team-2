﻿async function downloadSyllabusTemplate() {
    try {
        const response = await fetch('https://localhost:7000/api/syllabus/CSV/GetSyllabusTemplate');
        if (response.ok) {
            const blob = await response.blob();
            const url = window.URL.createObjectURL(blob);

            const link = document.createElement('a');
            link.href = url;
            link.download = 'SyllabusTemplate.csv';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        } else {
            console.error('Failed to fetch Syllabus template:', response.statusText);
        }
    } catch (error) {
        console.error('Error:', error);
    }
}

async function exportSyllabus() {
    try {
        const response = await fetch('https://localhost:7000/api/syllabus/CSV/GetSyllabusTemplate');
        if (response.ok) {
            const blob = await response.blob();
            const url = window.URL.createObjectURL(blob);

            const link = document.createElement('a');
            link.href = url;
            link.download = 'SyllabusTemplate.csv';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        } else {
            console.error('Failed to fetch Syllabus template:', response.statusText);
        }
    } catch (error) {
        console.error('Error:', error);
    }
}
async function exportAllSyllabus() {
    try {
        const response = await fetch('https://localhost:7000/api/syllabus/CSV/GetSyllabusTemplate');
        if (response.ok) {
            const blob = await response.blob();
            const url = window.URL.createObjectURL(blob);

            const link = document.createElement('a');
            link.href = url;
            link.download = 'SyllabusTemplate.csv';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        } else {
            console.error('Failed to fetch Syllabus template:', response.statusText);
        }
    } catch (error) {
        console.error('Error:', error);
    }
}

function importCSV() {
    const file = event.target.files[0];
    if (!file) {
        console.error('No file selected');
        return;
    }

    const formData = new FormData();
    formData.append('file', file);

    fetch('https://localhost:7000/api/syllabus/CSV/ImportSyllabus', {
        method: 'POST',
        body: formData
    })
        .then(response => response.json())
        .then(data => {
            console.log('Import successful:', data);
            // Handle successful import, e.g., display success message
        })
        .catch(error => console.error('Error importing CSV:', error));
}

document.addEventListener('DOMContentLoaded', function () {
    const downloadButton = document.getElementById('downloadLink');
    downloadButton.addEventListener('click', downloadSyllabusTemplate);
});