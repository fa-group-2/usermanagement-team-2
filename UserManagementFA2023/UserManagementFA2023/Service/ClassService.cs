﻿using UserManagementFA2023.Models.DTO;
using UserManagementFA2023.Models.Models.DTO;
using UserManagementFA2023.Service.IService;
using UserManagementFA2023.Utility;

namespace UserManagementFA2023.Service
{
	public class ClassService : IClassService
	{
		private readonly IBaseService _baseService;
		public ClassService(IBaseService baseService)
		{
			_baseService = baseService;
		}
		public async Task<ResponseDTO?> GetClass(int page, int pageSize)
		{
			return await _baseService.SendAsync(new RequestDTO()
			{
				ApiType = SD.ApiType.GET,
				Url = SD.ControllerAPI + "/api/class/" + page + "?pageSize=" + pageSize
            });
		}
        public async Task<ResponseDTO?> CreateClass(ClassDetailsDTO c)
        {
            return await _baseService.SendAsync(new RequestDTO()
            {
                ApiType = SD.ApiType.POST,
                Data = c,
                Url = SD.ControllerAPI + "/api/class/CreateClassDetails"
            }); ;
        }
    }
}
