﻿using UserManagementFA2023.Models.DTO;
using UserManagementFA2023.Models.Models.DTO;
using UserManagementFA2023.Service.IService;
using UserManagementFA2023.Utility;

namespace UserManagementFA2023.Service
{
    public class UserService : IUserService
    {
        private readonly IBaseService _baseService;
        public UserService(IBaseService baseService)
        {
            _baseService = baseService;
        }
        public async Task<ResponseDTO?> GetAllUser()
        {

            return await _baseService.SendAsync(new RequestDTO()
            {
                ApiType = SD.ApiType.GET,
                Url = SD.ControllerAPI + "/api/User/GetAllUser"
            });
        }
        public async Task<ResponseDTO?> GetUserByName(string name)
        {

            return await _baseService.SendAsync(new RequestDTO()
            {
                ApiType = SD.ApiType.GET,
                Url = SD.ControllerAPI + "/api/User/GetUserByName/" + name 
            });
        }
        public async Task<ResponseDTO?> UpdateUser(UserDTO user)
        {

            return await _baseService.SendAsync(new RequestDTO()
            {
                ApiType = SD.ApiType.POST,
                Data = user,    
                Url = SD.ControllerAPI + "/api/User/UpdateUser"
            });
        }
    }
}
