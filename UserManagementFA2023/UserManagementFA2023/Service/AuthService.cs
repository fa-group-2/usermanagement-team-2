﻿using UserManagementFA2023.Models.DTO;
using UserManagementFA2023.Models.Models.DTO;
using UserManagementFA2023.Service.IService;
using UserManagementFA2023.Utility;

namespace UserManagementFA2023.Service
{
    public class AuthService : IAuthService
    {
        private readonly IBaseService _baseService;
        public AuthService(IBaseService baseService)
        {
            _baseService = baseService; 
        }
        public async Task<ResponseDTO?> AssignRoleAsync(RegisterationRequestDTO registrationRequestDto)
        {
            return await _baseService.SendAsync(new RequestDTO()
            {
                ApiType = SD.ApiType.POST,
                Data = registrationRequestDto,
                Url = SD.ControllerAPI + "/api/auth/AssignRole"
            });
        }
        public async Task<ResponseDTO?> LoginAsync(LoginRequestDTO loginRequestDto)
        {
            return await _baseService.SendAsync(new RequestDTO()
            {
                ApiType = SD.ApiType.POST,
                Data = loginRequestDto,
                Url = SD.ControllerAPI + "/api/auth/login"
            }, withBearer: false);
        }

        public async Task<ResponseDTO?> RegisterAsync(RegisterationRequestDTO registerationRequestDTO)
        {
            return await _baseService.SendAsync(new RequestDTO()
            {
                ApiType = SD.ApiType.POST,
                Data = registerationRequestDTO,
                Url = SD.ControllerAPI + "/api/auth/register"
            }, withBearer: false);
        }
    }
}
