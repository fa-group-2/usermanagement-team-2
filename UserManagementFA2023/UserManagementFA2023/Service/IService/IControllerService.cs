﻿using UserManagementFA2023.Models.DTO;

namespace UserManagementFA2023.Service.IService
{
    public interface IControllerService
    {
        Task<ResponseDTO?> LoginAsync(LoginRequestDTO loginRequestDto);
        Task<ResponseDTO?> RegisterAsync(RegisterationRequestDTO registerationRequestDTO);
    }
}
