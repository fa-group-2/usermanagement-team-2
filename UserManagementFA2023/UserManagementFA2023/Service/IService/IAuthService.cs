﻿using UserManagementFA2023.Models.DTO;

namespace UserManagementFA2023.Service.IService
{
    public interface IAuthService
    {
        Task<ResponseDTO?> LoginAsync(LoginRequestDTO loginRequestDto);
        Task<ResponseDTO?> RegisterAsync(RegisterationRequestDTO registerationRequestDTO);
        Task<ResponseDTO?> AssignRoleAsync(RegisterationRequestDTO registerationRequestDTO);
    }
}
