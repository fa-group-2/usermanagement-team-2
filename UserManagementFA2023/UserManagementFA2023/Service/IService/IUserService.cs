﻿using UserManagementFA2023.Models.DTO;

namespace UserManagementFA2023.Service.IService
{
    public interface IUserService
    {
        Task<ResponseDTO?> GetAllUser();
        Task<ResponseDTO?> GetUserByName(string name);
        Task<ResponseDTO?> UpdateUser(UserDTO user);
    }
}
