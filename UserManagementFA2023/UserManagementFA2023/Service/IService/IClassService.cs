﻿using UserManagementFA2023.Models.DTO;

namespace UserManagementFA2023.Service.IService
{
	public interface IClassService
	{
		Task<ResponseDTO?> GetClass(int page, int pageSize);
        Task<ResponseDTO?> CreateClass(ClassDetailsDTO c);
    }
}
