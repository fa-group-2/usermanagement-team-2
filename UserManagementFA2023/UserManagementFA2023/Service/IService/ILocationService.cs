﻿using UserManagementFA2023.Models.DTO;

namespace UserManagementFA2023.Service.IService
{
    public interface ILocationService
    {
        Task<ResponseDTO?> GetAllLocation();
    }
}
