﻿using UserManagementFA2023.Models.DTO;

namespace UserManagementFA2023.Service.IService
{
    public interface ISyllabusService
    {
        Task<ResponseDTO?> GetAllSyllabus();
        Task<ResponseDTO?> CreateSyllabus(SyllabusDetailsDTO sy);
        Task<ResponseDTO?> GetSyllabusDetail(int id);
    }
}
