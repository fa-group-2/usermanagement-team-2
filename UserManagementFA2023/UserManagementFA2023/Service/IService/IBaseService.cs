﻿using UserManagementFA2023.Models.DTO;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.Service.IService
{
    public interface IBaseService
    {
        Task<ResponseDTO> SendAsync(RequestDTO requestDTO, bool withBearer = true);
    }
}
