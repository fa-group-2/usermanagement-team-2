﻿using UserManagementFA2023.Models.DTO;
using UserManagementFA2023.Models.Models.DTO;
using UserManagementFA2023.Service.IService;
using UserManagementFA2023.Utility;

namespace UserManagementFA2023.Service
{
    public class SyllabusService : ISyllabusService
    {
        private readonly IBaseService _baseService;
        public SyllabusService(IBaseService baseService)
        {
            _baseService = baseService;
        }
        public async Task<ResponseDTO?> GetAllSyllabus()
        {
            return await _baseService.SendAsync(new RequestDTO()
            {
                ApiType = SD.ApiType.GET,
                Url = SD.ControllerAPI + "/api/syllabus/GetAllSyllabus"
			});
        }
        public async Task<ResponseDTO?> CreateSyllabus(SyllabusDetailsDTO sy)
        {
            return await _baseService.SendAsync(new RequestDTO()
            {
                ApiType = SD.ApiType.POST,
                Data = sy,
                Url = SD.ControllerAPI + "/api/syllabus/CreateSyllabusDetails"
            }); ;
        }      

        public async Task<ResponseDTO?> GetSyllabusDetail(int id)
        {
            return await _baseService.SendAsync(new RequestDTO()
            {
                ApiType = SD.ApiType.GET,
                Url = SD.ControllerAPI + "/api/syllabus/GetSyllabusDetails/" + id
            }); 
        }
    }
}
