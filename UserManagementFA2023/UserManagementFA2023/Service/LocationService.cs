﻿using UserManagementFA2023.Models.DTO;
using UserManagementFA2023.Models.Models.DTO;
using UserManagementFA2023.Service.IService;
using UserManagementFA2023.Utility;

namespace UserManagementFA2023.Service
{
    public class LocationService : ILocationService
    {
        private readonly IBaseService _baseService;
        public LocationService(IBaseService baseService)
        {
            _baseService = baseService;
        }

        public async Task<ResponseDTO?> GetAllLocation()
        {
            return await _baseService.SendAsync(new RequestDTO()
            {
                ApiType = SD.ApiType.GET,
                Url = SD.ControllerAPI + "/api/Location/GetAllLocation"
            });
        }
    }
}
