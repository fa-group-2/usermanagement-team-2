﻿using UserManagementFA2023.Models.DTO;
using UserManagementFA2023.Models.Models.DTO;
using UserManagementFA2023.Service.IService;
using UserManagementFA2023.Utility;

namespace UserManagementFA2023.Service
{
    public class LevelService : ILevelService
    {
        private readonly IBaseService _baseService;
        public LevelService(IBaseService baseService)
        {
            _baseService = baseService;
        }
        public async Task<ResponseDTO?> GetAllLevel()
        {
            return await _baseService.SendAsync(new RequestDTO()
            {
                ApiType = SD.ApiType.GET,
                Url = SD.ControllerAPI + "/api/Level/GetAllLevel"
            });
        }
    }
}
