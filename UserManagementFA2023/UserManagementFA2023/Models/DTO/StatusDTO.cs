﻿using System.ComponentModel.DataAnnotations;

namespace UserManagementFA2023.Models.DTO
{
    public class StatusDTO
    {
        public int StatusId { get; set; } 
        public bool? IsActive { get; set; }  
     
    }
}
