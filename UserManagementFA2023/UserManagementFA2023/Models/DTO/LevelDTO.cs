﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.DTO    
{
    public class LevelDTO
    {
        public int LevelId { get; set; }
        public string? LevelName { get; set; }
    }
}
