﻿namespace UserManagementFA2023.Models.DTO
{
    public class ClassListViewDTO
    {
        public IEnumerable<ClassDTO>? classDTOs { get; set; }
        public IEnumerable<LocationDTO>? locationDTOs { get; set; }
        public IEnumerable<UserDTO>? userDTOs { get; set; }
        public IEnumerable<LevelDTO>? levelDTOs { get; set; }
        public IEnumerable<SyllabusDTO>? syllabusDTOs { get; set; }
        public PageDTO? pageDTO { get; set; }
    }
}
