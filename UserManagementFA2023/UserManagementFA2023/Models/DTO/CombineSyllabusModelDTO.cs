﻿namespace UserManagementFA2023.Models.DTO
{
    public class CombinedSyllabusModelDTO
    {
        public IEnumerable<SyllabusDTO>? syllabusDTOs { get; set; }
        public IEnumerable<StatusDTO>? statusDTOs { get; set; }
        public IEnumerable<UserDTO>? userDTOs { get; set; }
    }
}
