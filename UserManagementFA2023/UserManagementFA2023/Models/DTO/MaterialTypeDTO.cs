﻿using System.ComponentModel.DataAnnotations;

namespace UserManagementFA2023.Models.DTO
{
    public class MaterialTypeDTO
    {
        public int MaterialTypeId { get; set; } 
        public string? MaterialTypeName { get; set; }
    }
}
