﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.DTO
{
    public class TimelineDTO
    {
        public int TimeLineId { get; set; } 
        public int? SyllabusDetailId { get; set; }
        public SyllabusDetailsDTO? SyllabusDetails { get; set;}
        public string? Description { get; set; } 
        public int? UnitId { get; set; }
        public UnitDTO? Unit { get; set; }  
        public int? MatertialTypeId { get; set; }
        public MaterialTypeDTO? MaterialType { get; set; }  
        
    }
}
