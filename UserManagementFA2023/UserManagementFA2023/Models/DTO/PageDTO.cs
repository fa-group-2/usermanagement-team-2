﻿namespace UserManagementFA2023.Models.DTO
{
    public class PageDTO
    {
        public int  PageSize { get; set; }
        public int CurrentPageNumber { get; set; }
        public int? TotalPage { get; set; }
    }
}
