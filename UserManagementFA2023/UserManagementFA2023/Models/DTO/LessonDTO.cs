﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.DTO    
{
    public class LessonDTO
    {
        public int LessonId { get; set; } 
        public string? LessonName { get; set; }  
        public string? Description { get; set; } 
        public string? Method { get; set; }    
        public int? TraningTime { get; set; }    
        public int? UnitId { get; set; }
        public UnitDTO? Unit { get; set; }  
    }
}
