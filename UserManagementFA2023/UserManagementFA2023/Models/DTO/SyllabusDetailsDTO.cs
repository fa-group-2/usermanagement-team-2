﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.DTO
{
    public class SyllabusDetailsDTO
    {
        public int SyllabusDetailId { get; set; }
        public int? SyllabusId { get; private set; }
        public SyllabusDTO? Syllabus { get; set; }  
        public DateTime? LastModifyDate { get; set; }
        public string? Description { get; set;} 
        public string? RequirementDescription { get; set;}
        public int? LessonId { get; set;}
        public LessonDTO? Lesson { get; set; } 
        public int? LevelId { get; set;}
        public LevelDTO? Level { get; set;}  
        public int? AtendeeNumber { get; set;}   
    }
}
