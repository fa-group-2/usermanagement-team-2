﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.DTO
{
    public class UnitDTO
    {
        public int UnitId { get; set; }   
        public string? UnitName { get; set; }        
    }
}
