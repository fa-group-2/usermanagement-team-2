﻿using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.DTO
{
    public class TrainingProgramDTO
    {
        public int TrainningProgramId { get; set; }
        public string TrainningProgramName { get; set; }
        public string? Id { get; set; }
        public UserDTO? User { get; set; }
        public string Duration { get; set; }
        public int? StatusId { get; set; }
        public StatusDTO? Status { get; set; }
    }
}
