﻿namespace UserManagementFA2023.Models.DTO
{
    public class SyllabusInfoDTO
    {
        public SyllabusDetailsDTO syllabusDetail { get; set; }
        public IEnumerable<TimelineDTO> timelines { get; set; }
        
        public IEnumerable<UnitDTO> units { get; set; }
        public IEnumerable<LessonDTO> lessons { get; set; }

    }
}
