﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.DTO
{
    public class ClassDetailsDTO
    {
        public int ClassId { get; set; }
        [ForeignKey("ClassId")]
        public ClassDTO? Class { get; set; }
        [Key, Column(Order = 2)]
        public string? Id { get; set; }
        [ForeignKey("Id")]
        public UserDTO? User { get; set; }
      
    }
}
