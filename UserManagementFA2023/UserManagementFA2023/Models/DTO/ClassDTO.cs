﻿using Stripe.Terminal;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserManagementFA2023.Models.DTO    
{
    public class ClassDTO 
    {
        public int ClassId { get; set; }
        public string? ClassName { get; set; }
        public string? ClassCode { get; set; }
        public DateTime? DateCreate { get; set; }
        public int? LocationId { get; set; }
        public Location? Location { get; set; }
        public string? FSU { get; set; }
        public string? Status { get; set; }
        public string? Id { get; set; }
        public UserDTO? User { get; set; }
        public int? NumberOfAttendee { get; set; }
        public int LevelId { get; set; }
        [ForeignKey("LevelId")]
        public LevelDTO? Level { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public int? SyllabusId { get; set; }
        [ForeignKey("SyllabusId")]
        public SyllabusDTO? Syllabus { get; set; }
        public int TimeStart { get; set; }
        public int TimeEnd { get; set; }
    }
}
