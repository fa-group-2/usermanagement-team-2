﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.Utility
{

    public class MappingConfig
    {
        public static MapperConfiguration RegisterMaps()
        {
            var mappingCongif = new MapperConfiguration(config =>
            {
                config.CreateMap<Syllabus, SyllabusDTO>().ReverseMap();
                config.CreateMap<Class, Class>().ForMember(dest => dest.ClassId, opt => opt.Ignore());
				config.CreateMap<Class, ClassDTO>().ReverseMap();
				config.CreateMap<ClassDetails, ClassDetailsDTO>().ReverseMap();
                config.CreateMap<Lesson, LessonDTO>().ReverseMap();
                config.CreateMap<Level, LevelDTO>().ReverseMap();
                config.CreateMap<Location, LocationDTO>().ReverseMap();
                config.CreateMap<MaterialType, MaterialType>().ReverseMap();
                config.CreateMap<MaterialResource, MaterialResourceDTO>().ReverseMap();
                config.CreateMap<Status, StatusDTO>().ReverseMap();
                config.CreateMap<SyllabusDetails, SyllabusDetailsDTO>().ReverseMap();
                config.CreateMap<Unit, UnitDTO>().ReverseMap();
            });
            return mappingCongif;
        }
    }

}
