﻿using Microsoft.AspNetCore.Authorization.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagementFA2023.Utility.Helper
{
    public static class Operations
    {
        public static OperationAuthorizationRequirement Create =
     new OperationAuthorizationRequirement { Name = nameof(Create) };
        public static OperationAuthorizationRequirement View =
            new OperationAuthorizationRequirement { Name = nameof(View) };
        public static OperationAuthorizationRequirement FullAccess =
            new OperationAuthorizationRequirement { Name = nameof(FullAccess) };
        public static OperationAuthorizationRequirement Modify =
            new OperationAuthorizationRequirement { Name = nameof(Modify) };
    }
}
