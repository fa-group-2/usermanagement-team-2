﻿namespace UserManagementFA2023.API.Authorize
{
	public class Authority
	{
		public const string ROLE_VIEW = "VIEW";
		public const string ROLE_CREATE = "CREATE";
		public const string ROLE_ACCESS = "FULLACCESS";
		public const string ROLE_DENIED = "ACCESS DENIED";
		public const string ROLE_MODIFY = "MODIFY";
		public const string Permissions = "Permissions";
	}
}
