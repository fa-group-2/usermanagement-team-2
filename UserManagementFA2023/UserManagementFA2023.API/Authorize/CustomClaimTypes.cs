﻿namespace UserManagementFA2023.API.Authorize
{
    public static class CustomClaimTypes
    {
        public const string Syllabus = "Syllabus";
        public const string TrainingProgram = "TrainingProgram";
        public const string Class = "Class";
        public const string LearningMaterial = "LearningMaterial";
        public const string User = "User";
    }
}
