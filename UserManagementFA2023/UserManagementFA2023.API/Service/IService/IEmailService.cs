﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementFA2023.Models.Models;

namespace UserManagementFA2023.API.Service.IService
{
    public interface IEmailService
    {
        Task RegisterUserEmailAndLog(string email);
        Task EmailCreateClass(Class classDTO);
        Task EmailCreateSyllabus(Syllabus syllabusDTO);
    }
}
