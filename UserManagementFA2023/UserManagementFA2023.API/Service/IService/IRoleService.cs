﻿namespace UserManagementFA2023.API.Service.IService
{
    public interface IRoleService
    {
        Task<bool> CheckPermission(string functionId, string action, string[] roles);
    }
}
