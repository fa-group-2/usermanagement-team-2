﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using UserManagementFA2023.API.Service.IService;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models;

namespace UserManagementFA2023.API.Service
{
    public class RoleService : IRoleService
    {
        private RoleManager<IdentityRole> _roleManager;
        private readonly IFunctionRepository _functionRepository;
        private readonly IPermissionRepository _permissionRepository;
        public RoleService(RoleManager<IdentityRole> roleManager, IFunctionRepository functionRepository, IPermissionRepository permissionRepository)
        {
            _roleManager = roleManager; 
            _functionRepository = functionRepository;   
            _permissionRepository = permissionRepository;
        }
        public Task<bool> CheckPermission(string functionId, string action, string[] roles)
        {
            var functions = _functionRepository.FindAll();
            var permissions = _permissionRepository.FindAll();
            var query = from f in functions
                        join p in permissions on f.Id equals p.FunctionId
                        join r in _roleManager.Roles on p.RoleId equals r.Id
                        where roles.Contains(r.Name) && f.Id == functionId
                        && ((p.Create && action == "Create")
                        || (p.View && action == "View")
                        || (p.FullAccess && action == "FullAccess")
                        || (p.Modify && action == "Modify"))
                        select p;
            return query.AnyAsync();
        }
    }
}
