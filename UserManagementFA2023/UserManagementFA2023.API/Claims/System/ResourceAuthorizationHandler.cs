﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using System.Security.Claims;
using UserManagementFA2023.API.Service.IService;

namespace UserManagementFA2023.API.Claims.System
{
    public class ResourceAuthorizationHandler : AuthorizationHandler<OperationAuthorizationRequirement, string>
    {
        private readonly IRoleService _roleService;
        public ResourceAuthorizationHandler(IRoleService roleService)
        {
            _roleService = roleService; 
        }
        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, OperationAuthorizationRequirement requirement, string resource)
        {
            var roles = ((ClaimsIdentity)context.User.Identity).Claims.Where(x => x.Type.Equals("Permissions")).ToList();
            if (roles != null)
            {
                string[] animals = new string[roles.Count()];
                for (int i = 0; i < roles.Count(); i++)
                {
                    animals[i] = roles[i].Value;
                }
                var listRole = animals;
                var hasPermission = await _roleService.CheckPermission(resource, requirement.Name, listRole);
                if (hasPermission)
                {
                    context.Succeed(requirement);
                }
                else
                {
                    context.Fail();
                }
            }
            else
            {
                context.Fail();
            }
        }
    }
}
