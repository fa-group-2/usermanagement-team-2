﻿namespace UserManagementFA2023.API.RabbitMQSender
{
    public interface IRabbitMQControllerSenderMessageSender
    {
        void SendMessage(Object message, string queueName);
    }
}
