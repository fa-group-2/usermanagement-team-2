﻿using Azure;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Stripe;
using UserManagementFA2023.API.RabbitMQSender;
using UserManagementFA2023.DataAccess.Data;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.API.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ResponseDTO _response;
        private readonly AppDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;
        public UserController(IUnitOfWork unit, 
            IRabbitMQControllerSenderMessageSender messageBus, 
            IConfiguration configuration, AppDbContext db, UserManager<ApplicationUser> userManager)
        {
            _unitOfWork = unit;
            _response = new ResponseDTO();
            _db = db;   
            _userManager = userManager; 
        }
        [HttpGet]
        [Route("GetAllUser")]
        public async Task<ResponseDTO> GetAllUser()
        {
            return await _unitOfWork.userRepository.GetAll();
        }
        [HttpGet]
        [Route("GetUserByName/{name}")]
        public async Task<ResponseDTO> GetUserByName(string name)
        {
            return await _unitOfWork.userRepository.GetByName(name);    
        }
        [HttpPost("UpdateUser")]
        public async Task<ResponseDTO> UpdateUser([FromForm] UserDTO updateUser)
        {
            try
            {
                ApplicationUser user = _db.ApplicationUsers.FirstOrDefault(u => u.UserName == updateUser.Email);
                if (user != null)
                {
                    if (!string.IsNullOrEmpty(user.ImageLocalPath))
                    {
                        var oldFilePathDirectory = Path.Combine(Directory.GetCurrentDirectory(), user.ImageLocalPath);
                        FileInfo file = new FileInfo(oldFilePathDirectory);
                        if (file.Exists)
                        {
                            file.Delete();  
                        }
                    }
                    string fileName = user.UserName + Path.GetExtension(updateUser.Image.FileName);
                    string filePath = @"wwwroot\UserImages\" + fileName;
                    var filePathDirectory = Path.Combine(Directory.GetCurrentDirectory(), filePath);
                    using (var fileStream = new FileStream(filePathDirectory, FileMode.Create))
                    {
                        updateUser.Image.CopyTo(fileStream);    
                    }
                    var baseUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.Value}{HttpContext.Request.PathBase.Value}";
                    user.ImageUrl = baseUrl + "/ProductImages/" + fileName;
                    user.ImageLocalPath = filePath;
                }
                _db.ApplicationUsers.Update(user);
                _db.SaveChanges();

                _response.Result = user;
            }
            catch (Exception ex) {
                _response.IsSuccess = false;    
                _response.Message = ex.Message; 
            }
            return _response;   
        }
    }
}
