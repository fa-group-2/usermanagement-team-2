﻿using AutoMapper;
using Azure;
using CsvHelper;
using CsvHelper.Configuration;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using System.Formats.Asn1;
using System.Globalization;
using System.Net.Http.Headers;
using UserManagementFA2023.API.RabbitMQSender;
using UserManagementFA2023.DataAccess.Data;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.API.Controller
{
    [Route("api/syllabus")]
    [ApiController]
   // [Authorize("AllowFullAccess")]
    public class SyllabusController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ResponseDTO _response;
        private readonly IRabbitMQControllerSenderMessageSender _messageBus;
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly AppDbContext _db;
        private IMapper _mapper;
        public SyllabusController(IUnitOfWork unit, 
            AppDbContext db, 
            IRabbitMQControllerSenderMessageSender messageBus, 
            IConfiguration configuration, 
            IWebHostEnvironment webHostEnvironment,
            IMapper mapper)
        {
            _db = db;
            _unitOfWork = unit;
            _response = new ResponseDTO();
            _messageBus = messageBus;
            _configuration = configuration;
            _webHostEnvironment = webHostEnvironment;
            _mapper = mapper;   
        }

        [HttpGet]
        [Route("GetAllSyllabus")]
        public async Task<ResponseDTO> GetAllSyllabus()
        {
            return await _unitOfWork.syllabusRepository.GetAll();
        }
        [HttpGet]
        [Route("GetSyllabusById/{id:int}")]
        public async Task<ResponseDTO> GetSyllabusById(int id)
        {
            return await _unitOfWork.syllabusRepository.GetById(id);
        }
        [HttpPost]
        [Route("DuplicateSyllabus")]
        public void DuplicateSyllabus(int id)
        {
            _response.Result = _unitOfWork.syllabusRepository.DuplicatedSyllabus(id);
        }

        [HttpPost("EmailSyllabusRequest")]
        public async Task<object> EmailSyllabusRequest([FromBody] ClassDTO classDTO)
        {
            try
            {
                _messageBus.SendMessage(classDTO, _configuration.GetValue<string>("TopicAndQueueNames:SyllabusCreated"));
                _response.Result = true;
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.ToString();
            }
            return _response;
        }
        [HttpGet]
        [Route("CSV/GetSyllabusTemplate")]
        public async Task<IActionResult> GetSyllabusCSVTemplate()
        {
            var csvData = await _unitOfWork.syllabusRepository.DownloadSyllabusCSVTemplate();
            if (csvData == null)
            {
                return NotFound("Syllabus not found");
            }
            Response.Headers.Add("Content-Disposition", "attachment; filename=SyllabusTemplate.csv");
            Response.Headers.Add("Content-Type", "text/csv");

            return File(csvData, "text/csv");
        }

        [HttpPost]
        [Route("CSV/ImportSyllabus")]
        public async Task<ResponseDTO> ImportSyllabusCSV(IFormFile csvFile)
        {
            if (csvFile == null || csvFile.Length <= 0)
            {
                return new ResponseDTO { Message = "Invalid file length.", IsSuccess = false };
            }

            var fileExtension = Path.GetExtension(csvFile.FileName);
            if (fileExtension != ".csv")
            {
                return new ResponseDTO { Message = "Invalid file format. Only CSV File are supported!", IsSuccess = false };
            }

            using (var reader = new StreamReader(csvFile.OpenReadStream()))
            {
                var csvText = await reader.ReadToEndAsync();
                return await _unitOfWork.syllabusRepository.ImportSyllabusCSV(csvText);
            }
        }
        [HttpGet]
        [Route("CSV/ExportSyllabus/{syllabusId}")]
        public async Task<IActionResult> ExportSyllabusToCSV(int syllabusId)
        {
            var csvData = await _unitOfWork.syllabusRepository.ExportSyllabusCSV(syllabusId);

            Response.Headers.Add("Content-Disposition", $"attachment; filename=Syllabus_{syllabusId}.csv");
            Response.Headers.Add("Content-Type", "text/csv");

            return File(csvData, "text/csv");
        }
        [HttpGet]
        [Route("CSV/ExportAllSyllabus")]
        public async Task<IActionResult> ExportAllSyllabusToCSV()
        {
            var csvData = await _unitOfWork.syllabusRepository.ExportAllSyllabusCSV();

            if (csvData == null)
            {
                return NotFound("Syllabus not found");
            }
            Response.Headers.Add("Content-Disposition", $"attachment; filename=Syllabus_All.csv");
            Response.Headers.Add("Content-Type", "text/csv");

            return File(csvData, "text/csv");
        }
        [HttpPost]
        [Route("CreateSyllabus")]
        public async Task<ResponseDTO> CreateSyllabus(Syllabus obj)
        {
            _messageBus.SendMessage(obj, _configuration.GetValue<string>("TopicAndQueueNames:SyllabusCreatedTopic"));
            IEnumerable<ApplicationUser> users = _unitOfWork.userRepository.GetAllClass();
            obj.User = users.FirstOrDefault(u => u.UserName == obj.User.UserName);
            return await _unitOfWork.syllabusRepository.Create(obj);
        }
        [HttpPost]
        [Route("DeleteSyllabus/{id:int}")]
        public async Task<ResponseDTO> DeleteSyllabus(int id)
        {
            _messageBus.SendMessage(id, _configuration.GetValue<string>("TopicAndQueueNames:SyllabusDeleted"));
            return await _unitOfWork.syllabusRepository.ChangeStatus(id);
        }
        [HttpPost]
        [Route("RestoreSylabus")]
        public async Task<ResponseDTO> RestoreSyllabus(int id)
        {
            return await _unitOfWork.syllabusRepository.Restore(id);
        }
        [HttpPut]
        [Route("UpdateSyllabus")]
        public async Task<ResponseDTO> UpdateSyllabus(Syllabus syllabus)
        {
            return await _unitOfWork.syllabusRepository.Update(syllabus);
        }
        [HttpGet]
        [Route("GetSyllabusDetails/{id:int}")]
        public async Task<ResponseDTO> GetSyllabusDetailsById(int id)
        {
            try
            {
                SyllabusDetails syllabusDetails = _db.SyllabusDetails
                            .Include(sd=>sd.Level)
                            .FirstOrDefault(u => u.SyllabusId == id);
                if (syllabusDetails != null)
                {
                    syllabusDetails.Syllabus = _db.Syllabus
                    .Include(s => s.Status)
                    .Include(s => s.User)
                                    .FirstOrDefault(u => u.SyllabusId == id);
                }
                
                _response.Result = syllabusDetails; 
            }catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }
        [HttpPut]
        [Route("UpdateSyllabusDetails")]
        public async Task<ResponseDTO> UpdateSyllabusDetails(SyllabusDetails syllabusDetails)
        {
            return await _unitOfWork.syllabusDetailsRepository.Update(syllabusDetails);   
        }
        [HttpDelete]
        [Route("DeleteSyllabusDetails/{id:int}")]
        public async Task<ResponseDTO> DeleteSyllabusDetailsById(int id)
        {
            return await _unitOfWork.syllabusDetailsRepository.DeleteById(id);    
        }
        [HttpPost]
        [Route("CreateSyllabusDetails")]
        public async Task<ResponseDTO> CreateSyllabusDetails(SyllabusDetails syllabusDetails)
        {
            return await _unitOfWork.syllabusDetailsRepository.Create(syllabusDetails);   
        }
        [HttpGet]
        [Route("GetAllValidMaterialResources")]
        public async Task<IActionResult> GetAllMaterials()
        {
            var listMaterials = await _unitOfWork.materialRepository.GetAllValidMaterials();
            if (listMaterials != null)
            {
                return Ok(listMaterials);
            }
            else
            {
                return NotFound("Invalid materials!");
            }
        }

        [HttpPost]
        [Route("UploadMaterialFile")]
        public async Task<IActionResult> UploadMaterial (List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);
            var rootPath = Path.Combine(_webHostEnvironment.ContentRootPath, "Resources", "Learning Materials");

            if (!Directory.Exists(rootPath)) Directory.CreateDirectory(rootPath);

            foreach (var file in files)
            {
                var filePath = Path.Combine(rootPath, file.FileName);   

                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    var material = new MaterialResource
                    {
                        FileName = file.FileName,
                        ContentType = file.ContentType,
                        FileSize = file.Length,
                        UploadDate = DateTime.Now,
                        Status = true,
                    };

                    await file.CopyToAsync(stream);

                    _response.Result = _unitOfWork.materialRepository.Create(material);
                    _response.IsSuccess = true;
                }
            }
            return Ok(_response);
        }
        [HttpPost]
        [Route("DownloadMaterialFile/{id:int}")]
        public async Task<IActionResult> DownloadMaterial(int id)
        {
            var filePath = Path.Combine(_webHostEnvironment.ContentRootPath, "Resources", "Learning Materials");
            var downloadMaterial = await _unitOfWork.materialRepository.DownloadFileMaterialAsync(filePath, id);

            if (downloadMaterial != null)
            {
                return downloadMaterial;
            }
            else
            {
                return NotFound("Invalid or missing file!");
            }
        }
        [HttpDelete("DeleteMaterial/{id}")]
        public async Task<IActionResult> DeleteMaterial(int id)
        {
            try
            {
                var filePath = Path.Combine(_webHostEnvironment.ContentRootPath, "Resources", "Learning Materials");
                var isDeleted = await _unitOfWork.materialRepository.DeleteMaterialFile(id, filePath);

                if (isDeleted)
                {
                    return Ok("Material deleted successfully.");
                }
                else
                {
                    return NotFound("Material not found.");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("RemoveMaterialFromDatabase/{id:int}")]
        public async Task<ResponseDTO> RemoveMaterial(int id)
        {
            return await _unitOfWork.materialRepository.DeleteById(id);
        }
    }
}
