﻿using Azure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserManagementFA2023.API.RabbitMQSender;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.API.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ResponseDTO _response;
        private readonly IRabbitMQControllerSenderMessageSender _messageBus;
        private readonly IConfiguration _configuration;
        public LocationController(IUnitOfWork unit, IRabbitMQControllerSenderMessageSender messageBus, IConfiguration configuration)
        {
            _unitOfWork = unit;
            _response = new ResponseDTO();
            _messageBus = messageBus;
            _configuration = configuration;
        }
        [HttpGet]
        [Route("GetAllLocation")]
        public async Task<ResponseDTO> GetAllLocation()
        {
             
            return await _unitOfWork.locationRepository.GetAll(); ;
        }

    }
}
