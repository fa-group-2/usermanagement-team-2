﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using UserManagementFA2023.API.Authorize;
using UserManagementFA2023.API.RabbitMQSender;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models.DTO;
using UserManagementFA2023.Utility;

namespace UserManagementFA2023.API.Controller
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly ResponseDTO _response;
        private readonly IRabbitMQControllerSenderMessageSender _messageBus;
        private readonly IConfiguration _configuration;
        public readonly IAuthRepository _authRepository;
        private readonly RoleManager<IdentityRole> _roleManager;

        public AuthController(IUnitOfWork unit, IRabbitMQControllerSenderMessageSender messageBus, IConfiguration configuration, IAuthRepository authRepository, RoleManager<IdentityRole> roleManager)
        {
            _response = new ResponseDTO();
            _messageBus = messageBus;
            _configuration = configuration;
            _authRepository = authRepository;
            _roleManager = roleManager;
        }
        [HttpPost("login")]
        public async Task<ResponseDTO> Login(LoginRequestDTO loginRequestDTO)
        {
            _response.Result = await _authRepository.Login(loginRequestDTO);
            return _response;
        }
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterationRequestDTO model)
        {
            var errorMessage = await _authRepository.Register(model);
            if (!string.IsNullOrEmpty(errorMessage))
            {
                _response.IsSuccess = false;
                _response.Message = errorMessage;
                return BadRequest(_response);
            }
            _messageBus.SendMessage(model.Email, _configuration.GetValue<string>("TopicAndQueueNames:RegisterUserQueue"));
            return Ok(_response);
        }

        [HttpPost("AssignRole")]
        public async Task<IActionResult> AssignRole([FromBody] RegisterationRequestDTO model)
        {
            var assignRole = await _authRepository.AssignRole(model.Email, model.Role.ToUpper());
            if (!assignRole)
            {
                _response.IsSuccess = false;
                _response.Message = "Error encountered";
                return BadRequest(_response);
            }
            return Ok(_response);
        }

        [HttpPost("ChangeRole")]
        public async Task<IActionResult> ChangeRole([FromBody] string UserId, string NewRole)
        {
            _response.Result = _authRepository.ChangeUserRole(UserId, NewRole);
            return Ok(_response);
        }
        [HttpGet("GetRoleClaims/{roleName}")]
        public async Task<IActionResult> GetRoleClaims(string roleName)
        {
            var role = await _roleManager.FindByNameAsync(roleName);
            if (role == null)
            {
                return NotFound();
            }
            var roleClaims = await _roleManager.GetClaimsAsync(role);
            return Ok(roleClaims);
        }
        [HttpPost("UpdateUserClaim/{roleName}")]
        public async Task<ResponseDTO> UpdateUserClaim(string roleName, string claimType, string claimValue)
        {
            try
            {
                var role = await _roleManager.FindByNameAsync(roleName);
                if (role == null)
                {
                    _response.Message = "Not Found";
                }
                var roleClaims = await _roleManager.GetClaimsAsync(role);
                var existingClaim = roleClaims.FirstOrDefault(c => c.Type == claimType);
                if (existingClaim != null)
                {
                    var result = _roleManager.RemoveClaimAsync(role, existingClaim);
                }
                var claim = new Claim(claimType, claimValue);
                var addClaimResult = await _roleManager.AddClaimAsync(role, claim);
                _response.Result = addClaimResult;
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.ToString();
            }
            return _response;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> CreateClaimToRole(string roleName)
        {
            var userRole = await _roleManager.FindByNameAsync(roleName);
            if (userRole == null)
            {
                var result = await _roleManager.CreateAsync(userRole);
                if (result.Succeeded)
                {
                    await AddClaimsToRole(userRole);
                    return Ok();
                }
                else
                {
                    return StatusCode(500, result.Errors);
                }
            }
            else
            {
                await AddClaimsToRole(userRole);
                return Ok();
            }
        }
        private async Task AddClaimsToRole(IdentityRole role)
        {
            var claims = await _roleManager.GetClaimsAsync(role);
            if (false == claims.Any(p => p.Value.Equals(Authority.Permissions)))
            {
                await _roleManager.AddClaimAsync(role, new Claim(CustomClaimTypes.Syllabus, Authority.Permissions));
            }
            if (false == claims.Any(p => p.Value.Equals(Authority.Permissions)))
            {
                await _roleManager.AddClaimAsync(role, new Claim(CustomClaimTypes.Class, Authority.Permissions));
            }
            if (false == claims.Any(p => p.Value.Equals(Authority.Permissions)))
            {
                await _roleManager.AddClaimAsync(role, new Claim(CustomClaimTypes.TrainingProgram, Authority.Permissions));
            }
            if (false == claims.Any(p => p.Value.Equals(Authority.Permissions)))
            {
                await _roleManager.AddClaimAsync(role, new Claim(CustomClaimTypes.LearningMaterial, Authority.Permissions));
            }
            if (false == claims.Any(p => p.Value.Equals(Authority.Permissions)))
            {
                await _roleManager.AddClaimAsync(role, new Claim(CustomClaimTypes.User, Authority.Permissions));
            }
        }
    }
}
