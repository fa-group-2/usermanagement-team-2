﻿using Azure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UserManagementFA2023.API.RabbitMQSender;
using UserManagementFA2023.DataAccess.Data;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.API.Controller
{

    [Route("api/training")]
    [ApiController]
    public class TrainingController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ResponseDTO _response;
        private readonly IRabbitMQControllerSenderMessageSender _messageBus;
        private readonly IConfiguration _configuration;
        private readonly AppDbContext _db;
        public TrainingController(IUnitOfWork unit, IRabbitMQControllerSenderMessageSender messageBus, IConfiguration configuration, AppDbContext db)
        {
            _unitOfWork = unit;
            _response = new ResponseDTO();
            _messageBus = messageBus;
            _configuration = configuration;
            _db = db;
        }
        [HttpGet]
        [Route("GetAllTrainingProgram")]
        public async Task<ResponseDTO> GetAllTrainingProgram()
        {
            try
            {
                _response.Result = _db.TrainningPrograms
                    .Include(tp=>tp.User)
                    .Include(tp=>tp.Status)
                    .ToList();
            }catch(Exception ex)
            {
                _response.Message = ex.Message;
                _response.IsSuccess = false;
            }
            return _response;
        }
        [HttpGet]
        [Route("GetTrainingProgramDetails/{id:int}")]
        public async Task<ResponseDTO> GetTrainingProgram(int id)
        {
            return await _unitOfWork.trainingProgramRepository.GetById(id);
        }
        [HttpPut]
        [Route("UpdateTrainingProgram")]
        public async Task<ResponseDTO> UpdateTrainingProgram(TrainningProgram trainningProgram)
        {
            return await _unitOfWork.trainingProgramRepository.Update(trainningProgram);
        }
        [HttpPost]
        [Route("DeleteTrainingProgram/{id:int}")]
        public async Task<ResponseDTO> DeleteTrainingProgram(int id)
        {
            _response.Result = _unitOfWork.trainingProgramRepository.ChangeStatus(id);
            return _response;
        }
        [HttpPost]
        [Route("RestoreTrainingProgram")]
        public ResponseDTO RestoreTrainingProgram(int id)
        {
            _response.Result = _unitOfWork.trainingProgramRepository.Restore(id);
            return _response;
        }
        //create program
        [HttpPost]
        [Route("CreateTrainingProgram")]
        public ResponseDTO CreateTrainingProgram(TrainningProgram obj)
        {
            _response.Result = _unitOfWork.trainingProgramRepository.Create(obj);
            IEnumerable<ApplicationUser> users = _unitOfWork.userRepository.GetAllClass();
            obj.User = users.FirstOrDefault(u => u.UserName == obj.User.UserName);
            return _response;
        }
        //active program
        [HttpPost]
        [Route("{id}/Active")]
        public async Task<IActionResult> ActiveTrainingProgram(int id)
        {
            var response = await _unitOfWork.trainingProgramRepository.ActiveProgram(id);
            return Ok(response);
        }

        //deactive program
        [HttpPost]
        [Route("{id}/Deactivate")]
        public async Task<IActionResult> DeactivateTrainingProgram(int id)
        {
            var response = await _unitOfWork.trainingProgramRepository.DeactiveProgram(id);
            return Ok(response);
        }

        //search training program by name
        [HttpGet("Search")]
        public async Task<IActionResult> SearchTrainingPrograms(string keyword)
        {
            var programs = await _unitOfWork.trainingProgramRepository.SearchTrainingPrograms(keyword);
            return Ok(programs);
        }
    }
}

