﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserManagementFA2023.API.RabbitMQSender;
using UserManagementFA2023.DataAccess.Repository.IRepository;
using UserManagementFA2023.Models.Models;
using UserManagementFA2023.Models.Models.DTO;

namespace UserManagementFA2023.API.Controller
{
    [Route("api/class")]
    [ApiController]
    public class ClassController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ResponseDTO _response;
        private readonly IRabbitMQControllerSenderMessageSender _messageBus;
        private readonly IConfiguration _configuration;

        public ClassController(IUnitOfWork unit, IRabbitMQControllerSenderMessageSender messageBus, IConfiguration configuration)
        {
            _unitOfWork = unit;
            _response = new ResponseDTO();
            _messageBus = messageBus;
            _configuration = configuration;
        }
        [HttpPost]
        [Route("CreateClass")]
        public async Task<ResponseDTO> CreateClass([FromBody]Class obj)
        {
            _messageBus.SendMessage(obj, _configuration.GetValue<string>("TopicAndQueueNames:ClassCreatedTopic"));
            IEnumerable<ApplicationUser> users = _unitOfWork.userRepository.GetAllClass();
            obj.User = users.FirstOrDefault(u => u.UserName == obj.User.UserName);
            return await _unitOfWork.classRepository.Create(obj);
        }
        [HttpPost]
        [Route("DuplicateClass")]
        public async Task<ResponseDTO> DuplicateClass(int id)
        {
            return await _unitOfWork.classRepository.DuplicatedClass(id);
        }
        [HttpDelete]
        [Route("DeleteClass/{id:int}")]
        public async Task<ResponseDTO> DeleteClass(int id)
        {
            _messageBus.SendMessage(id, _configuration.GetValue<string>("TopicAndQueueNames:ClassDeleted"));
            return await _unitOfWork.classRepository.DeleteById(id);
        }
        [HttpGet]
        [Route("GetClass/{id:int}")]
        public async Task<ResponseDTO> GetClass(int id)
        {
            return await _unitOfWork.classRepository.GetById(id);
        }
        [HttpPut]
        [Route("UpdateClass")]
        public async Task<ResponseDTO> UpdateClass(Class obj)
        {
            return await _unitOfWork.classRepository.Update(obj);
        }

        [HttpGet]
        [Route("GetClassDetails/{id:int}")]
        public async Task<ResponseDTO> GetClassDetails(int id)
        {
            return await _unitOfWork.classDetailRepository.GetById(id);
        }

        [HttpPost]
        [Route("CreateClassDetails")]
        public async Task<ResponseDTO> CreateClassDetail(ClassDetails classDetails)
        {
            return await _unitOfWork.classDetailRepository.Create(classDetails);
        }
        [HttpPut]
        [Route("UpdateClassDetails")]
        public async Task<ResponseDTO> UpdateClassDetail(ClassDetails classDetails)
        {
            return await _unitOfWork.classDetailRepository.Update(classDetails);
        }
        [HttpDelete]
        [Route("DeleteClassDetail/{id:int}")]
        public async Task<ResponseDTO> DeleteClassDetail(int id)
        {
            return await _unitOfWork.classDetailRepository.DeleteById(id); ;
        }
        [HttpGet]
        [Route("{page:int}")]
        public async Task<ResponseDTO> PaginatedClass([FromQuery]int?[] locationId,
            DateTime? dateFrom, DateTime? dateTo, [FromQuery] int?[] attendeeId,
            int? userId, int page = 1, int pageSize = 3)
        {
            return await _unitOfWork.classRepository.GetAll(page, locationId, dateFrom, dateTo, attendeeId, userId, pageSize);
        }
    }
}
